
package com.landmark.wearable.uat.parser.model.slots;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "componentId",
    "products",
    "showNavigationArrows",
    "showRectImg",
    "showTitle",
    "title",
    "type"
})
public class Component {

    @JsonProperty("componentId")
    private String componentId;
    @JsonProperty("products")
    private List<Product> products = null;
    @JsonProperty("showNavigationArrows")
    private Boolean showNavigationArrows;
    @JsonProperty("showRectImg")
    private Boolean showRectImg;
    @JsonProperty("showTitle")
    private Boolean showTitle;
    @JsonProperty("title")
    private String title;
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("componentId")
    public String getComponentId() {
        return componentId;
    }

    @JsonProperty("componentId")
    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    @JsonProperty("products")
    public List<Product> getProducts() {
        return products;
    }

    @JsonProperty("products")
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @JsonProperty("showNavigationArrows")
    public Boolean getShowNavigationArrows() {
        return showNavigationArrows;
    }

    @JsonProperty("showNavigationArrows")
    public void setShowNavigationArrows(Boolean showNavigationArrows) {
        this.showNavigationArrows = showNavigationArrows;
    }

    @JsonProperty("showRectImg")
    public Boolean getShowRectImg() {
        return showRectImg;
    }

    @JsonProperty("showRectImg")
    public void setShowRectImg(Boolean showRectImg) {
        this.showRectImg = showRectImg;
    }

    @JsonProperty("showTitle")
    public Boolean getShowTitle() {
        return showTitle;
    }

    @JsonProperty("showTitle")
    public void setShowTitle(Boolean showTitle) {
        this.showTitle = showTitle;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
