package com.landmark.wearable.uat.network;

/**
 * Created by tushard on 24/02/16.
 */
public interface NetworkRequestCallback {

  void NetworkResultAvailable(LmsReqResp lmsReqResp);
}

