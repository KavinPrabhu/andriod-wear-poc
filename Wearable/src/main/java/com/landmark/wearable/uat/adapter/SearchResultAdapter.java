package com.landmark.wearable.uat.adapter;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.support.wearable.view.WearableRecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.landmark.wearable.uat.R;
import com.landmark.wearable.uat.contract.ProductItemOnClickListener;
import com.landmark.wearable.uat.parser.model.algolia.Facet;
import com.landmark.wearable.uat.parser.model.algolia.Product;
import java.util.ArrayList;

/**
 * Created by prasanth.p on 16/04/17.
 */

public class SearchResultAdapter extends
    WearableRecyclerView.Adapter<SearchResultAdapter.SearchHolder> {

  ArrayList<Facet> defaultFacets;
  ProductItemOnClickListener listener;
  public boolean isSearchContentAvailable;
  public ArrayList<Product> productsMatchedArray;
  //holder.price.setText(product.getPrice().getFormattedValue());


  public void setSearchContentAvailable(boolean searchContentAvailable) {
    isSearchContentAvailable = searchContentAvailable;
  }

  public void setProductsMatchedArray(ArrayList<Product> productsMatchedArray) {
    this.productsMatchedArray = productsMatchedArray;
  }


  public SearchResultAdapter(ArrayList<Facet> productList, ProductItemOnClickListener listener) {
    this.defaultFacets = productList;
    this.listener = listener;
  }


  @Override
  public SearchResultAdapter.SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.search_item, parent, false);
    return new SearchResultAdapter.SearchHolder(itemView);
  }


  @Override
  public void onBindViewHolder(SearchHolder holder, final int position) {

    if (isSearchContentAvailable) {
      final Product resultProduct = productsMatchedArray.get(position);
      Log.i("Prasanth", "productsMatchedArray size" + productsMatchedArray.size());
      Glide.with(holder.brandName.getContext()).load(resultProduct.imgLoRes)
          .thumbnail(0.5f)
          .crossFade()
          .into(holder.imageView);
      holder.imageView.setVisibility(View.VISIBLE);
      holder.brandName.setText(resultProduct.name);
    } else {
      final Facet product = defaultFacets.get(position);
      holder.brandName.setText(product.optionName);
    }

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (listener != null) {
          if (isSearchContentAvailable) {
            final Product resultProduct = productsMatchedArray.get(position);
            listener.onItemSelected(resultProduct.code);
          } else {
            final Facet facet = defaultFacets.get(position);
            listener.onCategoryItemSelected("/c/"+facet.optionValue);
          }
        }
      }
    });

  }

  /**
   * Returns the total number of items in the data set hold by the adapter.
   *
   * @return The total number of items in this adapter.
   */
  @Override
  public int getItemCount() {
    if (isSearchContentAvailable) {
      return productsMatchedArray == null ? 0 : productsMatchedArray.size();
    } else {
      return defaultFacets == null ? 0 : defaultFacets.size();
    }
  }

  class SearchHolder extends RecyclerView.ViewHolder {

    public TextView brandName;
    public TextView price;
    public AppCompatImageView imageView;

    public SearchHolder(View itemView) {
      super(itemView);
      brandName = (TextView) itemView.findViewById(R.id.product_name);
      price = (TextView) itemView.findViewById(R.id.product_price);
      imageView = (AppCompatImageView) itemView.findViewById(R.id.product_image);
    }
  }
}
