package com.landmark.wearable.uat.view;

import static com.landmark.wearable.uat.App.EXTRA_PRODUCT_ID;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.wearable.view.WearableRecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import com.landmark.wearable.uat.Constants;
import com.landmark.wearable.uat.R;
import com.landmark.wearable.uat.adapter.SearchResultAdapter;
import com.landmark.wearable.uat.algolia.Categoryhelper;
import com.landmark.wearable.uat.algolia.SearchAutoCompleteContract.SearchView;
import com.landmark.wearable.uat.algolia.SearchAutoCompletePresenter;
import com.landmark.wearable.uat.contract.ProductItemOnClickListener;
import com.landmark.wearable.uat.parser.model.algolia.Facet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by prasanth.p on 10/05/17.
 */

public class SearchActivity extends Activity implements SearchView, TextWatcher,
    ProductItemOnClickListener {

  private static final int SPEECH_REQUEST_CODE = 0;
  SearchAutoCompletePresenter presenter;
  Categoryhelper objHelper = new Categoryhelper();
  WearableRecyclerView list;
  private Pattern PATTERN;
  private String searchTxt;
  EditText editText;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.search_activity);
    objHelper.disjunctiveFacets = new ArrayList<String>();
    objHelper.refinements = new HashMap<>();
    presenter = new SearchAutoCompletePresenter(this);
    presenter.onAlgoliaCall(objHelper);
    list = (WearableRecyclerView) findViewById(R.id.search_result_view);
    editText = (EditText) findViewById(R.id.search);
    editText.addTextChangedListener(this);
    ImageView voiceSearch = (ImageView) findViewById(R.id.voice_search);
    voiceSearch.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        displaySpeechRecognizer();
      }
    });
  }

  @Override
  public void algoliaCall(Categoryhelper mHelper, ArrayList<Facet> catFacetsArray) {
    final SearchResultAdapter searchResultAdapter = new SearchResultAdapter(catFacetsArray, this);
    if (catFacetsArray != null && !catFacetsArray.isEmpty() &&
        mHelper.productsMatchedArray != null &&
        !mHelper.productsMatchedArray.isEmpty()) {
      if (!TextUtils.isEmpty(searchTxt)) {
        searchResultAdapter.setSearchContentAvailable(true);
        searchResultAdapter.setProductsMatchedArray(mHelper.productsMatchedArray);
      }
    } else {
      searchResultAdapter.setSearchContentAvailable(false);
    }
    RecyclerView.LayoutManager manager = new LinearLayoutManager(getBaseContext(),
        LinearLayoutManager.VERTICAL, false);
    list.setLayoutManager(manager);
    list.setAdapter(searchResultAdapter);
    searchResultAdapter.notifyDataSetChanged();
  }


  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after) {

  }


  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count) {
    searchTxt = s.toString();
    if (s.length() > 0) {
      PATTERN = Pattern.compile(Constants.PATTERN_ENGLISH);
      Matcher matcher = PATTERN.matcher(s);
      if (matcher.matches()) {
        objHelper.searchQry = s.toString();
        presenter.onAlgoliaCall(objHelper);
      }
    }
  }


  @Override
  public void afterTextChanged(Editable s) {

  }

  @Override
  public void onItemSelected(String productId) {
    Intent intent = new Intent(this, ProductDetailsActivity.class);
    intent.putExtra(EXTRA_PRODUCT_ID, productId);
    startActivity(intent);
  }

  @Override
  public void onCategoryItemSelected(String selection) {
    Intent intent = new Intent(this, CategoryActivity.class);
    intent.putExtra("URL", selection);
    intent.putExtra("FragTag", "Category");
    startActivity(intent);
  }

  // Create an intent that can start the Speech Recognizer activity
  private void displaySpeechRecognizer() {
    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
// Start the activity, the intent will be populated with the speech text
    startActivityForResult(intent, SPEECH_REQUEST_CODE);
  }

  // This callback is invoked when the Speech Recognizer returns.
// This is where you process the intent and extract the speech text from the intent.
  @Override
  protected void onActivityResult(int requestCode, int resultCode,
      Intent data) {
    if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
      List<String> results = data.getStringArrayListExtra(
          RecognizerIntent.EXTRA_RESULTS);
      String spokenText = results.get(0);
      editText.setText(spokenText);
    }
    super.onActivityResult(requestCode, resultCode, data);
  }
}
