package com.landmark.wearable.uat.algolia;


import com.landmark.wearable.uat.Constants;
import com.landmark.wearable.uat.parser.model.algolia.Badge;
import com.landmark.wearable.uat.parser.model.algolia.Facet;
import com.landmark.wearable.uat.parser.model.algolia.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tushard on 24/03/16.
 */
public class AlgoliaHelper {

  public static boolean isLandscape = true;

  // ------- Algolia Parsers ------
  public static ArrayList<Facet> getCategoryList(String categoryCode, JSONObject jsonObject) {
    if (jsonObject == null) {
      return null;
    }
    ArrayList<Facet> cfArray = new ArrayList<Facet>();
    String patternStr = "(([a-z]||[A-Z]||[0-9])+)#.*";            // The pattern is modified below based on categoryCode
    Pattern pattern;
    Matcher matcher;

    JSONObject jsFacets = null;
    try {
      jsFacets = jsonObject.getJSONObject("facets").getJSONObject(
          "categoryFacetValue" + "." + "en");
      if (jsFacets == null) {
        return null;
      }

      if (categoryCode != null && !categoryCode.isEmpty()) {
        patternStr = "^" + categoryCode + "-"
            + patternStr;        // Final pattern generated eg - ^women-tops-(([a-z]||[A-Z])+)#.*
      } else {
                /*This condition is used for brand specific navigation which is not in production 27 feb 2017 once up i have to update config file to true.*/
        if (/*AppConfig.isBrand*/ false) {
          patternStr = "^"
              + "(([a-z]||[A-Z])+)#.*false";                             // Final pattern generated for brands- ^(([a-z]||[A-Z])+)#.*
        } else {
          patternStr = "^"
              + patternStr;                             // Final pattern generated - ^(([a-z]||[A-Z])+)#.*
        }
      }

      pattern = Pattern.compile(patternStr, Pattern.MULTILINE);

      Iterator<String> keys = jsFacets.keys();
      while (keys.hasNext()) {
        String entry = keys.next();
        Integer hit = jsFacets.getInt(entry);

        matcher = pattern.matcher(entry);
        if (matcher.matches()) {
          // split on hash - get categoryCode
          String[] tmpArr = entry.split("#");
          String category = tmpArr[0];
          String categoryName = tmpArr[1];

          // Save the facet
          Facet cfValue = new Facet();
          cfValue.optionName = categoryName;
          cfValue.optionValue = category;
          cfValue.priority = hit;
          cfValue.selected = false;
          cfArray.add(cfValue);
        }
      }
    } catch (JSONException e) {
      // e.printStackTrace();
    }

    // Do alphabetical sortNO
    Collections.sort(cfArray, Facet.getFacetNameComparator());

    return cfArray;
  }

  public static ArrayList<Product> parseProducts(JSONObject jsonObject, boolean useRectangularImage,
      String language) {
//   new method for landscape images public static ArrayList<Product> parseProducts (JSONObject jsonObject, String imageFormat) {

    if (jsonObject == null) {
      return null;
    }

    ArrayList<Product> allProducts = new ArrayList<Product>();
    try {
      JSONArray jsHits = jsonObject.getJSONArray("hits");
      if (jsHits == null) {
        return null;
      }

      for (int i = 0; i < jsHits.length(); i++) {
        JSONObject jsProd = jsHits.getJSONObject(i);
        if (jsProd == null) {
          continue;
        }

        Product prod = new Product();
        prod.name = jsProd.optJSONObject("name").getString(language);
        prod.code = jsProd.getString("objectID");
        prod.url = jsProd.getString("url");
        prod.concept = jsProd.optJSONObject("concept").getString(language);
        prod.price = jsProd.getInt("price");

        if (jsProd.has("wasPrice")) {
          prod.basePrice = jsProd.getInt("wasPrice");
        }
        if (jsProd.has("employeePrice")) {
          prod.employeePrice = jsProd.getDouble("employeePrice");
        }
        prod.currencyIso = Constants.currencyISO;

        JSONArray jsBadgeArray = jsProd.optJSONArray("badge");
        if (jsBadgeArray != null) {
          prod.badges = new ArrayList<>();
          for (int k = 0; k < jsBadgeArray.length(); k++) {
            JSONObject jsBadge = jsBadgeArray.getJSONObject(k);

            Badge badge = new Badge();
            badge.code = jsBadge.optString("code");
            if (jsBadge.has("title")) {
              badge.title = jsBadge.optJSONObject("title").getString(language);
            }
            badge.position = jsBadge.optInt("position", k);
            badge.cssStyle = jsBadge.optString("cssStyle", "green");
            badge.visibility = jsBadge.optBoolean("visibility", true);

            if (badge.visibility) {
              prod.badges.add(badge);
            }
          }
          Collections.sort(prod.badges);
        }

        if (useRectangularImage) {
          prod.imgHiRes = jsProd.optString(Constants.PORTRAIT_RESOLUTION);
          prod.isRectangle = true;
        }
        if (prod.imgHiRes == null || prod.imgHiRes.isEmpty()) {
          prod.imgHiRes = jsProd.optString(Constants.SQUARE_RESOLUTION);
        }
        prod.imgLoRes = jsProd.optString(Constants.LOW_RESOLUTION);

        // changing logic based on new flag 'imageFormat'
                /*if(imageFormat != null && !imageFormat.isEmpty()) {
                    if (imageFormat.equalsIgnoreCase(AppConstants.IMAGE_TYPE_LANDSCAPE)) {
                        prod.imgHiRes = jsProd.optString(AppConstants.LANDSCAPE_RESOLUTION);
                        prod.imageFormat = AppConstants.IMAGE_TYPE_LANDSCAPE;
                    } else if (imageFormat.equalsIgnoreCase(AppConstants.IMAGE_TYPE_PORTRAIT)) {
                        prod.imgHiRes = jsProd.optString(AppConstants.PORTRAIT_RESOLUTION);
                        prod.imageFormat = AppConstants.IMAGE_TYPE_PORTRAIT;
                    } else {
                        prod.imgHiRes = jsProd.optString(AppConstants.SQUARE_RESOLUTION);
                        prod.imageFormat = AppConstants.IMAGE_TYPE_SQUARE;
                    }
                } else{
                        prod.imgHiRes = jsProd.optString(AppConstants.SQUARE_RESOLUTION);
                        prod.imageFormat = AppConstants.IMAGE_TYPE_SQUARE;
                }
                prod.imgLoRes = jsProd.optString(AppConstants.LOW_RESOLUTION) ;*/

        allProducts.add(prod);
      }

    } catch (JSONException e) {
      e.printStackTrace();
    }

    return allProducts;
  }

  public static ArrayList<Product> parseProducts(JSONObject jsonObject,
      String language, boolean useRectangularImage, String imageFormat) {

    if (jsonObject == null) {
      return null;
    }

    ArrayList<Product> allProducts = new ArrayList<Product>();
    try {
      JSONArray jsHits = jsonObject.getJSONArray("hits");
      if (jsHits == null) {
        return null;
      }

      for (int i = 0; i < jsHits.length(); i++) {
        JSONObject jsProd = jsHits.getJSONObject(i);
        if (jsProd == null) {
          continue;
        }

        Product prod = new Product();
        prod.name = jsProd.optJSONObject("name").getString(language);
        prod.code = jsProd.getString("objectID");
        prod.url = jsProd.getString("url");
        prod.concept = jsProd.optJSONObject("concept").getString(language);
        prod.price = jsProd.getInt("price");

        if (jsProd.has("wasPrice")) {
          prod.basePrice = jsProd.getInt("wasPrice");
        }
        if (jsProd.has("employeePrice")) {
          prod.employeePrice = jsProd.getDouble("employeePrice");
        }
        prod.currencyIso = "AED";

        JSONArray jsBadgeArray = jsProd.optJSONArray("badge");
        if (jsBadgeArray != null) {
          prod.badges = new ArrayList<>();
          for (int k = 0; k < jsBadgeArray.length(); k++) {
            JSONObject jsBadge = jsBadgeArray.getJSONObject(k);

            Badge badge = new Badge();
            badge.code = jsBadge.optString("code");
            if (jsBadge.has("title")) {
              badge.title = jsBadge.optJSONObject("title").getString(language);
            }
            badge.position = jsBadge.optInt("position", k);
            badge.cssStyle = jsBadge.optString("cssStyle", "green");
            badge.visibility = jsBadge.optBoolean("visibility", true);

            if (badge.visibility) {
              prod.badges.add(badge);
            }
          }
          Collections.sort(prod.badges);
        }

        if (!isLandscape) {
          if (useRectangularImage) {
            prod.imgHiRes = jsProd.optString(Constants.PORTRAIT_RESOLUTION);
            prod.isRectangle = true;
          }
          if (prod.imgHiRes == null || prod.imgHiRes.isEmpty()) {
            prod.imgHiRes = jsProd.optString(Constants.SQUARE_RESOLUTION);
          }
          prod.imgLoRes = jsProd.optString(Constants.LOW_RESOLUTION);
        } else {
          // changing logic based on new flag 'imageFormat'
          if (imageFormat != null && !imageFormat.isEmpty()) {
            if (imageFormat.equalsIgnoreCase(Constants.IMAGE_TYPE_LANDSCAPE)) {
              prod.imgHiRes = jsProd.optString(Constants.LANDSCAPE_RESOLUTION);
              prod.imageFormat = Constants.IMAGE_TYPE_LANDSCAPE;
            } else if (imageFormat.equalsIgnoreCase(Constants.IMAGE_TYPE_PORTRAIT)) {
              prod.imgHiRes = jsProd.optString(Constants.PORTRAIT_RESOLUTION);
              prod.imageFormat = Constants.IMAGE_TYPE_PORTRAIT;
            } else {
              prod.imgHiRes = jsProd.optString(Constants.SQUARE_RESOLUTION);
              prod.imageFormat = Constants.IMAGE_TYPE_SQUARE;
            }
          } else {
            prod.imgHiRes = jsProd.optString(Constants.SQUARE_RESOLUTION);
            prod.imageFormat = Constants.IMAGE_TYPE_SQUARE;
          }
          prod.imgLoRes = jsProd.optString(Constants.LOW_RESOLUTION);
        }
        allProducts.add(prod);
      }


    } catch (JSONException e) {
      e.printStackTrace();
    }

    return allProducts;
  }

  public static ArrayList<Product> parseSearchProducts(JSONObject jsonObject,
      boolean useRectangularImage) {

    if (jsonObject == null) {
      return null;
    }

    ArrayList<Product> allProducts = new ArrayList<Product>();
    try {
      JSONArray jsHits = jsonObject.getJSONArray("hits");
      if (jsHits == null) {
        return null;
      }

      for (int i = 0; i < jsHits.length(); i++) {
        JSONObject jsProd = jsHits.getJSONObject(i);
        if (jsProd == null) {
          continue;
        }

        Product prod = new Product();
        prod.name = jsProd.optJSONObject("name").getString(Constants.lang);
        prod.code = jsProd.getString("objectID");
        prod.url = jsProd.getString("url");
        prod.concept = jsProd.getString("concept");
        prod.imgLoRes = jsProd.optString("50WX50H");

        allProducts.add(prod);
      }

    } catch (JSONException e) {
      e.printStackTrace();
    }

    return allProducts;
  }

  public static int getMatchedProducts(JSONObject jsonObject) {

    if (jsonObject == null) {
      return -1;
    }

    int productsMatched = 0;
    try {
      productsMatched = jsonObject.getInt("nbHits");
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return productsMatched;
  }

}
