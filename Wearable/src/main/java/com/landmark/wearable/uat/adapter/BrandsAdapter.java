package com.landmark.wearable.uat.adapter;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.support.wearable.view.WearableRecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.landmark.wearable.uat.R;
import com.landmark.wearable.uat.contract.ProductItemOnClickListener;
import com.landmark.wearable.uat.parser.model.slots.Product;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by prasanth.p on 16/04/17.
 */

public class BrandsAdapter extends WearableRecyclerView.Adapter<BrandsAdapter.BrandsHolder> {
    List<Product> productList;
    ProductItemOnClickListener listener;

    public BrandsAdapter(List<Product> productList, ProductItemOnClickListener listener) {
        this.productList = productList;
        this.listener = listener;
    }

    /**
     * Called when RecyclerView needs a new {@link ViewHolder} of the given type to represent
     * an item.
     * <p>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ViewHolder, int, List)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to an
     *                 adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ViewHolder, int)
     */
    @Override
    public BrandsAdapter.BrandsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item, parent, false);
        return new BrandsHolder(itemView);
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ViewHolder#itemView} to reflect the item at the given
     * position.
     * <p>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     *
     * Override {@link #onBindViewHolder(ViewHolder, int, List)} instead if Adapter can
     * handle effcient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the item
     *                 at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(BrandsHolder holder, int position) {
        final Product product = productList.get(position);

        holder.brandName.setText(product.getName());
        holder.price.setText(product.getPrice().getFormattedValue());
        Glide.with(holder.brandName.getContext()).load(product.getImages().get(0).getUrl())
                .thumbnail(0.5f)
                .crossFade()
                .into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemSelected(product.getCode());
            }
        });
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return productList == null ? 0 : productList.size();
    }

    class BrandsHolder extends RecyclerView.ViewHolder {
        public TextView brandName;
        public TextView price;
        public AppCompatImageView imageView;


        public BrandsHolder(View itemView) {
            super(itemView);
            brandName = (TextView) itemView.findViewById(R.id.product_name);
            price = (TextView) itemView.findViewById(R.id.product_price);
            imageView = (AppCompatImageView) itemView.findViewById(R.id.product_image);
        }
    }
}
