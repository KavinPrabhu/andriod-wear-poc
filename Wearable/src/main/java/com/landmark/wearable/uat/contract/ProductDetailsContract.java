package com.landmark.wearable.uat.contract;

import com.landmark.wearable.uat.parser.model.productdetails.ProductDetails;

/**
 * Created by prasanth.p on 04/05/17.
 */

public interface ProductDetailsContract {

  public interface EventHandler {

    void getProductDetails(String productID);

    void addtoFavorite(String productcode);

    void removefromFavorite(String productcode);

    void addProducttoCart(String product, String quantity);
  }

  public interface ProductDetailsView {

    void onProductDetailsAvailable(ProductDetails productDetails);

    void hideProgressBar();

    void showSuccessScreen();

    void showProgressBar(String message);
  }
}
