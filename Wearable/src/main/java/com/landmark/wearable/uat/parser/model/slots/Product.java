
package com.landmark.wearable.uat.parser.model.slots;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "approved",
    "badges",
    "baseProduct",
    "brand",
    "categories",
    "code",
    "concept",
    "conceptDelivery",
    "description",
    "images",
    "manufacturer",
    "name",
    "potentialPromotions",
    "price",
    "purchasable",
    "threshold",
    "url",
    "variantType",
    "wishListProduct",
    "averageRating"
})
public class Product {

    @JsonProperty("approved")
    public Boolean approved;
    @JsonProperty("badges")
    public List<Badge> badges = null;
    @JsonProperty("baseProduct")
    public String baseProduct;
    @JsonProperty("brand")
    public Brand brand;
    @JsonProperty("categories")
    public List<Category> categories = null;
    @JsonProperty("code")
    public String code;
    @JsonProperty("concept")
    public Concept concept;
    @JsonProperty("conceptDelivery")
    public Boolean conceptDelivery;
    @JsonProperty("description")
    public String description;
    @JsonProperty("images")
    public List<Image> images = null;
    @JsonProperty("manufacturer")
    public String manufacturer;
    @JsonProperty("name")
    public String name;
    @JsonProperty("potentialPromotions")
    public List<PotentialPromotion> potentialPromotions = null;
    @JsonProperty("price")
    public Price price;
    @JsonProperty("purchasable")
    public Boolean purchasable;
    @JsonProperty("threshold")
    public Integer threshold;
    @JsonProperty("url")
    public String url;
    @JsonProperty("variantType")
    public String variantType;
    @JsonProperty("wishListProduct")
    public Boolean wishListProduct;
    @JsonProperty("averageRating")
    public Integer averageRating;
    @JsonIgnore
    public Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("approved")
    public Boolean getApproved() {
        return approved;
    }

    @JsonProperty("approved")
    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    @JsonProperty("badges")
    public List<Badge> getBadges() {
        return badges;
    }

    @JsonProperty("badges")
    public void setBadges(List<Badge> badges) {
        this.badges = badges;
    }

    @JsonProperty("baseProduct")
    public String getBaseProduct() {
        return baseProduct;
    }

    @JsonProperty("baseProduct")
    public void setBaseProduct(String baseProduct) {
        this.baseProduct = baseProduct;
    }

    @JsonProperty("brand")
    public Brand getBrand() {
        return brand;
    }

    @JsonProperty("brand")
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("concept")
    public Concept getConcept() {
        return concept;
    }

    @JsonProperty("concept")
    public void setConcept(Concept concept) {
        this.concept = concept;
    }

    @JsonProperty("conceptDelivery")
    public Boolean getConceptDelivery() {
        return conceptDelivery;
    }

    @JsonProperty("conceptDelivery")
    public void setConceptDelivery(Boolean conceptDelivery) {
        this.conceptDelivery = conceptDelivery;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("images")
    public List<Image> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    @JsonProperty("manufacturer")
    public String getManufacturer() {
        return manufacturer;
    }

    @JsonProperty("manufacturer")
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("potentialPromotions")
    public List<PotentialPromotion> getPotentialPromotions() {
        return potentialPromotions;
    }

    @JsonProperty("potentialPromotions")
    public void setPotentialPromotions(List<PotentialPromotion> potentialPromotions) {
        this.potentialPromotions = potentialPromotions;
    }

    @JsonProperty("price")
    public Price getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Price price) {
        this.price = price;
    }

    @JsonProperty("purchasable")
    public Boolean getPurchasable() {
        return purchasable;
    }

    @JsonProperty("purchasable")
    public void setPurchasable(Boolean purchasable) {
        this.purchasable = purchasable;
    }

    @JsonProperty("threshold")
    public Integer getThreshold() {
        return threshold;
    }

    @JsonProperty("threshold")
    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("variantType")
    public String getVariantType() {
        return variantType;
    }

    @JsonProperty("variantType")
    public void setVariantType(String variantType) {
        this.variantType = variantType;
    }

    @JsonProperty("wishListProduct")
    public Boolean getWishListProduct() {
        return wishListProduct;
    }

    @JsonProperty("wishListProduct")
    public void setWishListProduct(Boolean wishListProduct) {
        this.wishListProduct = wishListProduct;
    }

    @JsonProperty("averageRating")
    public Integer getAverageRating() {
        return averageRating;
    }

    @JsonProperty("averageRating")
    public void setAverageRating(Integer averageRating) {
        this.averageRating = averageRating;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
