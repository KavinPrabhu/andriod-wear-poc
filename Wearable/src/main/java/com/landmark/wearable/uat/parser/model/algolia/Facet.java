package com.landmark.wearable.uat.parser.model.algolia;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tushard on 27/03/16.
 */
public class Facet implements Comparable<Facet> {

  // Compare sizes
  private static Map<String, Integer> sizeMap;
  public String facetTitle;
  public String facetName;
  public String className;
  public String optionName;
  public String optionValue;
  public Boolean selected;
  public Integer priority;
  // Price specific fields
  public Integer minPrice;
  public Integer maxPrice;
  public ArrayList<Facet> facetList;

  // Compare alphabetically
  public static Comparator<Facet> getFacetNameComparator() {
    return new Comparator<Facet>() {
      @Override
      public int compare(Facet lhs, Facet rhs) {
        if (lhs.optionName != null && rhs.optionName != null) {
          return lhs.optionName.compareTo(rhs.optionName);
        } else {
          return 0;
        }
      }
    };
  }

  public static Comparator<Facet> getFacetSizeAlphaComparator() {
    return new Comparator<Facet>() {
      @Override
      public int compare(Facet lhs, Facet rhs) {

        if (lhs.optionName == null || rhs.optionName == null) {
          return 0;
        }

        // Size comparison map
        if (sizeMap == null) {
          sizeMap = new HashMap<>();
          sizeMap.put("XXS", 1);
          sizeMap.put("XS", 2);
          sizeMap.put("S", 3);
          sizeMap.put("S/M", 4);
          sizeMap.put("M", 5);
          sizeMap.put("L", 6);
          sizeMap.put("L/XL", 7);
          sizeMap.put("XL", 8);
          sizeMap.put("XXL", 9);
          sizeMap.put("XXL", 10);
          sizeMap.put("3XL", 11);
          sizeMap.put("4XL", 12);
        }

        Integer lhsWeight = sizeMap.get(lhs.optionName);
        Integer rhsWeight = sizeMap.get(rhs.optionName);

        if (lhsWeight != null && rhsWeight != null) {
          return lhsWeight.compareTo(rhsWeight);
        } else {
          return 0;
        }
      }
    };
  }

  public static Comparator<Facet> getFacetSizeNumericComparator() {
    return new Comparator<Facet>() {
      @Override
      public int compare(Facet lhs, Facet rhs) {

        if (lhs.optionName == null || rhs.optionName == null) {
          return 0;
        }

        try {
          Double lhsNumeric = Double.valueOf(lhs.optionName);
          Double rhsNumeric = Double.valueOf(rhs.optionName);

          return lhsNumeric.compareTo(rhsNumeric);
        } catch (NumberFormatException e) {
          return 0;
        }
      }
    };
  }

  // Default comparator
  @Override
  public int compareTo(Facet another) {
    return another.priority.compareTo(this.priority);
  }

}
