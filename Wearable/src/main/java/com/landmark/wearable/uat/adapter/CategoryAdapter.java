package com.landmark.wearable.uat.adapter;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.support.wearable.view.WearableRecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.landmark.wearable.uat.R;
import com.landmark.wearable.uat.contract.ProductItemOnClickListener;
import com.landmark.wearable.uat.parser.model.algolia.Product;
import java.util.List;


/**
 * Created by prasanth.p on 16/04/17.
 */

public class CategoryAdapter extends WearableRecyclerView.Adapter<CategoryAdapter.BrandsHolder> {

  List<Product> productList;
  ProductItemOnClickListener listener;

  public CategoryAdapter(List<Product> productList, ProductItemOnClickListener listener) {
    this.productList = productList;
    this.listener = listener;
  }


  @Override
  public CategoryAdapter.BrandsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item, parent, false);
    return new BrandsHolder(itemView);
  }


  @Override
  public void onBindViewHolder(BrandsHolder holder, int position) {
    final Product product = productList.get(position);
    holder.brandName.setText(product.name);
    holder.price.setText(product.priceFormattedValue);
    Glide.with(holder.brandName.getContext()).load(product.imgHiRes)
        .thumbnail(0.5f)
        .crossFade()
        .into(holder.imageView);
    holder.imageView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        listener.onItemSelected(product.code);
      }
    });
  }

  /**
   * Returns the total number of items in the data set hold by the adapter.
   *
   * @return The total number of items in this adapter.
   */
  @Override
  public int getItemCount() {
    return productList == null ? 0 : productList.size();
  }

  class BrandsHolder extends RecyclerView.ViewHolder {

    public TextView brandName;
    public TextView price;
    public AppCompatImageView imageView;


    public BrandsHolder(View itemView) {
      super(itemView);
      brandName = (TextView) itemView.findViewById(R.id.product_name);
      price = (TextView) itemView.findViewById(R.id.product_price);
      imageView = (AppCompatImageView) itemView.findViewById(R.id.product_image);
    }
  }
}
