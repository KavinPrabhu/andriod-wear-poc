package com.landmark.wearable.uat.algolia;


import static com.landmark.wearable.uat.Constants.algIndexMaping;
import static com.landmark.wearable.uat.Constants.algoliaProductIndex;

import com.algolia.search.saas.AlgoliaException;
import com.algolia.search.saas.Client;
import com.algolia.search.saas.CompletionHandler;
import com.algolia.search.saas.Index;
import com.algolia.search.saas.Query;
import com.fasterxml.jackson.databind.JsonNode;
import com.landmark.wearable.uat.Constants;
import com.landmark.wearable.uat.network.LmsReqResp;
import com.landmark.wearable.uat.network.NetworkRequestCallback;
import com.landmark.wearable.uat.parser.model.algolia.Product;
import com.landmark.wearable.uat.parser.model.algolia.RequiredFacet;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pratikbehera on 1/5/17.
 * <p>
 * Presenter class which implements the business logic to be executed when an event
 * is triggered from CategoryPg_SearchPg.
 */

public class CategoryAndSearchPresenter implements CategorySearchContract.EventHandler,
    NetworkRequestCallback {

  // Algolia
  Client algClient;
  Index algIndex;
  private CategorySearchContract.CategoryPgView mCategoryPgView = null;
  private int productsMatched;
  private ArrayList<Product> allProducts;
  private ArrayList<Product> newProducts;
  private Categoryhelper objhelper;
  private String searchRequiredFacets =
      "{\"facets\":[{\"collapsed\":true,\"name\":\"sizeNumeric\",\"className\":\"size\"," +
          "\"maxThreshold\":10,\"title\":{\"en\":\"Size - Numeric\"},\"priority\":40}," +
          "{\"collapsed\":true,\"name\":\"sizeSystem\",\"className\":\"size\",\"sort\":\"Y\"," +
          "\"maxThreshold\":10,\"title\":{\"en\":\"Size - Alpha\"},\"priority\":39}," +
          "{\"collapsed\":true,\"name\":\"sizeMonth\",\"className\":\"size\"," +
          "\"maxThreshold\":10,\"title\":{\"en\":\"Month\"},\"priority\":4000}," +
          "{\"collapsed\":true,\"name\":\"sizeYear\",\"className\":\"size\"," +
          "\"maxThreshold\":10,\"title\":{\"en\":\"Year\"},\"priority\":4000}," +
          "{\"collapsed\":true,\"name\":\"color.en\",\"className\":\"color.en\",\"maxThreshold\":10,"
          +
          "\"title\":{\"en\":\"Color\"},\"priority\":59}]}";

  public static final String JSON_KEY_NAME = "name";
  public static final String JSON_KEY_FIRST_LEVEL_CATEGORY_CODE = "firstLevelCategoryCode";
  public static final String JSON_KEY_IS_RECTANGULAR_IMAGE = "isRectangularImage";

  static {
    algIndexMaping = new LinkedHashMap<String, String>();
    algIndexMaping.put("name", "_by_asc_name.en");
    algIndexMaping.put("price", "_by_asc_price");
    algIndexMaping.put("price-revers", "_by_desc_price");
    algIndexMaping.put("recent-products", "_by_desc_createDate");
  }

  public CategoryAndSearchPresenter(CategorySearchContract.CategoryPgView CategoryPgView) {
    mCategoryPgView = CategoryPgView;
  }

  /**
   * Function to do operations when Activity gets created.
   */

  @Override
  public void onActivityCreated(Categoryhelper mhelper) {

    this.objhelper = mhelper;
    objhelper.isPageLoading = false;
    // Get Query parameters
    objhelper.queryParams = new HashMap<>();
    if (objhelper.fragment_url != null) {
      parseQryStr(objhelper.fragment_url);

      // Algolia init
      algClient = new Client(Constants.algoliaAppId, Constants.algoliaAPIKey);
      if (objhelper.algoliaSelectedIndex == null || objhelper.algoliaSelectedIndex.isEmpty()) {
        objhelper.algoliaSelectedIndex = algoliaProductIndex;
      }

      if (objhelper.disjunctiveFacets == null || objhelper.refinements == null) {
        addDefaultFacets();
      }

      if (objhelper.categoryCode != null && !objhelper.categoryCode.isEmpty()) {
        sendAlgoliaCategoryReq();
      } else {
        sendAlgoliaProdReq();
      }
    } else {
      mCategoryPgView.showSearchFailureError();
    }
  }

  /**
   * Function to do operations when Activity gets result.
   */
  @Override
  public void onActivityResult(Categoryhelper mhelper) {
    this.objhelper = mhelper;

    if ((objhelper.newCategoryCode != null && objhelper.categoryCode == null) || (
        objhelper.newCategoryCode != null && objhelper.categoryCode != null
            && !objhelper.categoryCode.equals(objhelper.newCategoryCode))) {
      String subCode = null;
      objhelper.pageNo = 0;    // Reset page no
      String newSearchQuery;
      if (objhelper.fragment_url.contains("/search") && objhelper.fragment_url
          .contains("allPromotions")) {
        if (objhelper.searchQry != null) {
          newSearchQuery =
              "/search?q=" + objhelper.searchQry + ":allCategories:" + objhelper.newCategoryCode;
        } else {
          newSearchQuery = "/search?q=allCategories:" + objhelper.newCategoryCode;
        }
        if (objhelper.refinements != null) {
          List<String> allpromo = objhelper.refinements.get("allPromotions");
          if (allpromo != null && !allpromo.isEmpty()) {
            for (String promo : allpromo) {
              newSearchQuery = newSearchQuery + ":allPromotions:" + promo;
            }
          }
        }
      } else if (objhelper.searchQry == null || objhelper.searchQry.isEmpty()) {
        String concept = null;
        List<String> arrayList = objhelper.queryParams.get("concept");
        if (arrayList != null && arrayList.size() > 0) {
          concept = arrayList.get(0);
        }
        if (concept != null) {
          newSearchQuery = "/c/" + objhelper.newCategoryCode + "?q=concept:" + concept;
        } else {
          if (objhelper.fragment_url.contains("badge.title:Clearance") || objhelper.fragment_url
              .contains("/search?q=badge.title%3AClearance")) {
            newSearchQuery = "/c/" + objhelper.newCategoryCode + "?q=badge.title:Clearance";
          } else {
            newSearchQuery = "/c/" + objhelper.newCategoryCode;
          }
        }
      } else {
        newSearchQuery =
            "/search?q=" + objhelper.searchQry + ":allCategories:" + objhelper.newCategoryCode;
      }
      objhelper.filterApplied = false;
      mCategoryPgView.CallNewCategoryFragment(objhelper, newSearchQuery);
    } else {
      // Reload page even if filter applied or not
      objhelper.pageNo = 0;    // Reset page no
      sendAlgoliaProdReq();
    }

  }

  /**
   * Function to Add Default facets.
   */
  private void addDefaultFacets() {
    objhelper.disjunctiveFacets = new ArrayList<String>();
    objhelper.refinements = new HashMap<>();

    List<String> instocklist = new ArrayList<>();
    instocklist.add(Constants.algoliaInStock);
    objhelper.disjunctiveFacets.add("inStock");
    objhelper.refinements.put("inStock", instocklist);

    List<String> approvallist = new ArrayList<>();
    approvallist.add(Constants.algoliaApprovalStatus);
    objhelper.disjunctiveFacets.add("approvalStatus");
    objhelper.refinements.put("approvalStatus", approvallist);
    if (objhelper.queryParams != null) {
      Iterator iterator = objhelper.queryParams.entrySet().iterator();
      while (iterator.hasNext()) {
        Map.Entry pair = (Map.Entry) iterator.next();

        String key = pair.getKey().toString();
        List<String> list = (List<String>) pair.getValue();
        objhelper.disjunctiveFacets.add(key);
        objhelper.refinements.put(key, list);
      }
    }

    if (objhelper.categoryCode != null && !objhelper.categoryCode.isEmpty()) {
      if (objhelper.disjunctiveFacets != null && objhelper.refinements != null) {
        List<String> categorylist;
        categorylist = objhelper.refinements.get("allCategories");
        if (categorylist == null) {
          categorylist = new ArrayList<>();
        }
        categorylist.add(objhelper.categoryCode);
        objhelper.refinements.put("allCategories", categorylist);

        if (!objhelper.disjunctiveFacets.contains("allCategories")) {
          objhelper.disjunctiveFacets.add("allCategories");
        }
      }
    }

    if (objhelper.fragmentTag != null && objhelper.fragmentTag.equalsIgnoreCase("CATEGORY")
        && objhelper.disjunctiveFacets != null && !objhelper.disjunctiveFacets
        .contains("badge.title")) {
      List<String> clearancelist = new ArrayList<>();
      clearancelist.add("-Clearance");
      objhelper.disjunctiveFacets.add("badge.title.en");
      objhelper.refinements.put("badge.title.en", clearancelist);
    }

  }

  /**
   * Function to Send Category Algolia calls.
   */
  private void sendAlgoliaCategoryReq() {
    algIndex = algClient.initIndex(Constants.algoliaCategoryIndex);

    algIndex.getObjectAsync(objhelper.categoryCode, new CompletionHandler() {
      @Override
      public void requestCompleted(JSONObject jsonObject, AlgoliaException error) {
        if (error != null) {
          sendAlgoliaProdReq();
        } else {
          // Parse Category Index
          if (jsonObject != null) {
            objhelper.requiredFacets = new ArrayList<RequiredFacet>();
            String lang = "en";

            try {

              objhelper.firstLevelCatCode = jsonObject
                  .optString(JSON_KEY_FIRST_LEVEL_CATEGORY_CODE);
              objhelper.useRectangularImage = jsonObject
                  .optBoolean(JSON_KEY_IS_RECTANGULAR_IMAGE);
              objhelper.imageFormat = jsonObject
                  .optString("imageFormat"); //used for landscape image
              objhelper.categoryTitle = jsonObject.optJSONObject(JSON_KEY_NAME)
                  .getString(lang);

              // GA ScreenView
              String gtmScreenName = "";
              if (objhelper.firstLevelCatCode != null) {
                gtmScreenName = objhelper.firstLevelCatCode;
              }
              if (objhelper.categoryTitle != null) {
                gtmScreenName = gtmScreenName + " > " + objhelper.categoryTitle;
              }
              Map<String, Object> gtmMap = new HashMap<>();
              gtmMap.put("screenName", gtmScreenName);
              //LmsAnalytics.gtmPushEvent(AppController.getInstance(), "OpenScreen", gtmMap);

              JSONArray jsFacets = jsonObject.getJSONArray("facets");
              for (int i = 0; i < jsFacets.length(); i++) {
                JSONObject jsItem = jsFacets.getJSONObject(i);

                RequiredFacet facet = new RequiredFacet();
                facet.className = jsItem.optString("className", "other");
                facet.collapsed = jsItem.optBoolean("collapsed", true);
                facet.maxThreshold = jsItem.optInt("maxThreshold", 10);
                facet.facetName = jsItem.optString("name", "");
                facet.facetTitle = jsItem.optJSONObject("title").optString(lang);
                facet.priority = jsItem.optInt("priority", 800);

                objhelper.requiredFacets.add(facet);
              }
              Collections.sort(objhelper.requiredFacets);

            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
          // Send request to Product Index
          sendAlgoliaProdReq();
        }
      }
    });
  }

  /**
   * Function to Set Required facets for search.
   */
  private ArrayList<RequiredFacet> setRequiredFacetsForSearch() {
    ArrayList<RequiredFacet> requiredFacets = new ArrayList<>();
    try {
      if (searchRequiredFacets == null || searchRequiredFacets.isEmpty()) {
        return requiredFacets;
      }

      JSONObject jsonObject = new JSONObject(searchRequiredFacets);
      JSONArray jsFacets = jsonObject.getJSONArray("facets");

      for (int i = 0; i < jsFacets.length(); i++) {
        JSONObject jsItem = jsFacets.getJSONObject(i);

        RequiredFacet facet = new RequiredFacet();
        facet.className = jsItem.optString("className", "other");
        facet.collapsed = jsItem.optBoolean("collapsed", true);
        facet.maxThreshold = jsItem.optInt("maxThreshold", 10);
        facet.facetName = jsItem.optString("name", "");
        facet.facetTitle = jsItem.optJSONObject("title")
            .optString("en");
        facet.priority = jsItem.optInt("priority", 800);

        requiredFacets.add(facet);
      }
      Collections.sort(requiredFacets);
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return requiredFacets;
  }

  /**
   * Function to Send  Algolia calls.
   */
  private void sendAlgoliaProdReq() {

    // GA ScreenView
    Map<String, Object> gtmMap = new HashMap<>();
    gtmMap.put("screenName", "SearchResults");
    //LmsAnalytics.gtmPushEvent(AppController.getInstance(), "OpenScreen", gtmMap);

    objhelper.requiredFacets = setRequiredFacetsForSearch();

    List<String> attributesToRetrive = new ArrayList<String>();
    attributesToRetrive.add(
        "concept,manufacturerName,url,333WX493H,345WX345H,505WX316H,badge,name,wasPrice,price,employeePrice,showMoreColor");
//        attributesToRetrive.add("*");

    Query query;
    if (objhelper.searchQry != null && !objhelper.searchQry.isEmpty()) {
      query = new Query(objhelper.searchQry);
    } else {
      query = new Query();
    }
    query.setHitsPerPage(objhelper.pageHits);
    query.setPage(objhelper.pageNo);
    query.setFacets("*");
    query.setGetRankingInfo(true);

    if (objhelper.tagFilter != null) {
      query.setTagFilters(objhelper.tagFilter);
    }

    JSONArray priceFilter = new JSONArray();
    if (objhelper.nowMinPrice > 0 && objhelper.nowMaxPrice > 0
        && objhelper.nowMaxPrice >= objhelper.nowMinPrice) {
      priceFilter.put("price: " + objhelper.nowMinPrice + " to " + objhelper.nowMaxPrice);
    } else {
      priceFilter.put("price > 1");
    }
    query.setNumericFilters(priceFilter);
    query.setAttributesToRetrieve("concept", "manufacturerName", "url", "333WX493H", "345WX345H",
        "505WX316H", "badge", "name", "wasPrice", "price", "employeePrice", "showMoreColor");

    if (objhelper.algoliaSelectedIndex != null && !objhelper.algoliaSelectedIndex.isEmpty()) {
      algIndex = algClient.initIndex(objhelper.algoliaSelectedIndex);
    } else {
      algIndex = algClient.initIndex(algoliaProductIndex);
    }

    /**
     * Perform a search with disjunctive facets, generating as many queries as number of disjunctive facets.
     *
     * @param query             The query.
     * @param disjunctiveFacets List of disjunctive facets.
     * @param refinements       The current refinements, mapping facet names to a list of values.
     * @param completionHandler The listener that will be notified of the request's outcome.
     * @return A cancellable request.
     */
    algIndex
        .searchDisjunctiveFacetingAsync(query, objhelper.disjunctiveFacets, objhelper.refinements,
            new CompletionHandler() {
              @Override
              public void requestCompleted(JSONObject jsonObject, AlgoliaException error) {
                if (error != null) {
                  mCategoryPgView.showApiError();
                } else {
                  if (jsonObject == null) {
                    return;
                  }
                  if (!objhelper.isPageLoading) {
                    productsMatched = AlgoliaHelper.getMatchedProducts(jsonObject);
                    objhelper.productsMatched = productsMatched;
//                    allProducts = AlgoliaHelper
//                        .parseProducts(jsonObject, objhelper.useRectangularImage,
//                            LocaleUtils.getLanguage(AppController.getInstance()));
                    allProducts = AlgoliaHelper.parseProducts(jsonObject,
                        "en",
                        objhelper.useRectangularImage,
                        objhelper.imageFormat); //used for landscape images
                    mCategoryPgView.updateCategorydata(allProducts, objhelper);
                  } else {
                    productsMatched = AlgoliaHelper.getMatchedProducts(jsonObject);
                    objhelper.productsMatched = productsMatched;
                    newProducts = AlgoliaHelper
                        .parseProducts(jsonObject, "en",
                            objhelper.useRectangularImage, objhelper.imageFormat);
                    //                 newProducts = AlgoliaHelper.parseProducts(jsonObject,
                    // objhelper.imageFormat); used for landscape images
                    if (newProducts != null && newProducts.size() > 0) {

                      if (allProducts == null) {
                        allProducts = new ArrayList<>();
                      }
                      allProducts.addAll(newProducts);
                    } else {
                      objhelper.pageDataEnd = true;
                    }
                    mCategoryPgView.updateCategorydata(allProducts, objhelper);
                  }
                }
              }
            });

  }

  /**
   * Function to Parse Search query string.
   */
  private void parseQryStr(String searchCategoryUrl) {

    String url = null;
    url = searchCategoryUrl;

    // Split with / and get the last str - that may be categoryCode
    String[] urlsplit = url.split("/");

    String tmp = urlsplit[urlsplit.length - 1];
    String code = tmp.split("\\?")[0];
    if (!code.equalsIgnoreCase("search")) {
      // code is not equal to search - this is category url
      objhelper.categoryCode = code;
    }

    // Try parsing query parameters if any
    String queryStr = null;
    String[] tmpqry = url.split("\\?");

    if (tmpqry.length > 1) {
      queryStr = url.split("\\?")[1];
    }
    if (queryStr != null && !queryStr.isEmpty()) {
      // We have a valid query string - parse it
      String[] qryPairs = queryStr.split("&");
      for (int i = 0; i < qryPairs.length; i++) {
        String[] item = qryPairs[i].split("=");

        String key = "";
        String val = "";
        try {
          key = URLDecoder.decode(item[0], "UTF-8");
        } catch (Exception e) {
          key = item[0];
        }
        if (item.length > 1) {
          try {
            val = URLDecoder.decode(item[1], "UTF-8");
          } catch (Exception e) {
            val = item[1];
          }
        }

        // Right now we are only concerned with q= string we dont need any other part of query string
        if (key.equalsIgnoreCase("q")) {
          // Parse the search query parameters
          int j = 0;
          val = val.replaceAll("%3A", ":");
          String[] elem = val.split(":");
          if ((elem.length % 2) == 1) {
            // We have odd no of : pairs - assume the 1st one to be search string
            objhelper.searchQry = elem[0];
            j = 1;
          }
          if (elem.length >= 2) {
            // Parse only if we have more items in array
            String algfacet;
            for (; j < elem.length; j = j + 2) {
              // Log.d (TAG, " Elem --> " + elem[j] + " : " + elem[j+1]) ;
                            /*
                            if ( elem[j].equalsIgnoreCase("allCategories") ) {
                                categoryCode = elem[j+1] ;
                            } else
                            */
              if (elem[j].equalsIgnoreCase("index")) {
                String useIndex = elem[j + 1];
                if (useIndex != null && !useIndex.isEmpty()) {
                  objhelper.sortOption = algIndexMaping.get(useIndex);
                  if (objhelper.sortOption != null) {
                    objhelper.algoliaSelectedIndex =
                        Constants.algoliaProductIndex + objhelper.sortOption;
                  } else {
                    objhelper.algoliaSelectedIndex = Constants.algoliaProductIndex;
                  }
                }
              }
                            /*
                            else if ( elem[j].equalsIgnoreCase("category") ) {
                                categoryCode = elem[j+1] ;
                            }
                             */
              else {
                algfacet = elem[j] + ":" + elem[j + 1];
                // Log.d(TAG, "algfacet --> " + algfacet) ;
                List<String> arrayList;
                arrayList = objhelper.queryParams.get(elem[j]);
                if (arrayList == null) {
                  arrayList = new ArrayList<>();
                }
                arrayList.add(elem[j + 1]);
                objhelper.queryParams.put(elem[j], arrayList);
              }
            }
          }
        }
                /*
                if ( key.equalsIgnoreCase("allCategories")) {
                    categoryCode = val ;
                }
                */
        if (key.equalsIgnoreCase("price")) {
          String priceRange = val;
          String[] fromTo;
          if (priceRange != null) {
            try {
              fromTo = val.split(",");
              if (fromTo != null && fromTo.length >= 1) {
                objhelper.minPrice = Integer.valueOf(fromTo[0]);
                objhelper.nowMinPrice = objhelper.minPrice;
              }
              if (fromTo != null && fromTo.length >= 2) {
                objhelper.maxPrice = Integer.valueOf(fromTo[1]);
                objhelper.nowMaxPrice = objhelper.maxPrice;
              }
            } catch (Exception e) {
            }
          }
        }

      }
    }
  }

  /**
   * Function to do actions when the view is destroyed.
   */
  @Override
  public void onViewDestroyed() {
    /*AppController.cancelAllRequests("AllFavoriteCategory");
    AppController.cancelAllRequests("addFavorite");
    AppController.cancelAllRequests("removeFavorite");*/
  }

  /**
   * Function to do operations to call Algolia.
   */
  @Override
  public void onAlgoliaCall(Categoryhelper mhelper) {
    this.objhelper = mhelper;
    sendAlgoliaProdReq();
  }

  /**
   * Function to do operations to get all Favourite Product code.
   */
  @Override
  public void oncallFavourites() {
    //StandardApiReq.getAllFavoriteCategory(AppController.getInstance(), this);
  }

  /**
   * Function to do operations when Product add to Favourite.
   */
  @Override
  public void onAddtoFavourite(String strCode) {
    //StandardApiReq.addtoFavorite(strCode, this);
  }

  /**
   * Function to do operations when Product remove from Favourite.
   */
  @Override
  public void onRemoveFavourite(String strCode) {
    //StandardApiReq.removefromFavorite(strCode, this);
  }


  /**
   * API Request Volley Callback.
   */
  @Override
  public void NetworkResultAvailable(LmsReqResp lmsReqResp) {
    if (lmsReqResp != null && lmsReqResp.respStatusCode == 200) {
      if (lmsReqResp != null && lmsReqResp.methodName.equals("AllFavoriteCategory")) {
        /**
         * Response of All Favorite product code .
         */
        JsonNode jsAllNodes = lmsReqResp.respJSON.path("productCode");
        mCategoryPgView.allFavoriteCategory(jsAllNodes);
      } else if (lmsReqResp != null && lmsReqResp.methodName.equals("addFavorite")) {
        /**
         * Response After adding product to favorite .
         */
        if (lmsReqResp.respJSON.has("success")) {
          if (lmsReqResp.respJSON.path("success").asBoolean()) {
            mCategoryPgView.addFavorite();
          } else {

          }
        }
      } else if (lmsReqResp != null && lmsReqResp.methodName.equals("removeFavorite")) {
        /**
         * Response After Removing  product to favorite .
         */
        if (lmsReqResp.respJSON.has("success")) {
          if (lmsReqResp.respJSON.path("success").asBoolean()) {
            mCategoryPgView.removeFavorite();
          } else {

          }
        }
      }
    }
  }
}
