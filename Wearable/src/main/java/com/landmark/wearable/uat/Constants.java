package com.landmark.wearable.uat;

import java.util.Map;

/**
 * Created by prasanth.p on 10/05/17.
 */

public class Constants {

  public static String lang = "en";
  public static String currencyISO = "\u20B9";
  // ---- Algolia ---
  // Algolia Keys
  public static String algoliaAPIKey = "4c4f62629d66d4e9463ddb94b9217afb";
  public static String algoliaAppId = "3HWOWX4270";
  public static String algoliaInStock = "1";
  public static String algoliaApprovalStatus = "1";
  public static String login_req_text = "Please login to check favourites.";
  // Algolia Indexes
  public static Map<String, String> algIndexPostfix;
  public static Map<String, String> algIndexMaping;
  public static String algoliaIndexPrefix = "uat2_uae";
  public static String algoliaProductIndex = "uat2_uae_Product";
  public static String algoliaCategoryIndex = "uat2_uae_Category";

  public static final String PATTERN_ENGLISH = "[ a-zA-Z0-9%&]+";

  public static final String IMAGE_TYPE_LANDSCAPE = "LANDSCAPE";
  public static final String IMAGE_TYPE_PORTRAIT = "PORTRAIT";
  public static final String IMAGE_TYPE_SQUARE = "SQUARE";
  public static final String LANDSCAPE_RESOLUTION = "505WX316H";
  public static final String PORTRAIT_RESOLUTION = "333WX493H";
  public static final String SQUARE_RESOLUTION = "345WX345H";
  public static final String LOW_RESOLUTION = "50WX50H";


}
