package com.landmark.wearable.uat.parser.model.algolia;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ajayk on 01/03/16.
 */
public class Product implements Parcelable {

  public static final Creator<Product> CREATOR = new Creator<Product>() {
    @Override
    public Product createFromParcel(Parcel in) {
      return new Product(in);
    }

    @Override
    public Product[] newArray(int size) {
      return new Product[size];
    }
  };
  public String name;
  public String code;
  public String url;
  public String variantType;
  public boolean purchasable;
  public String concept;
  public String conceptName;
  public Integer price;
  public String priceFormattedValue;
  public Integer basePrice;
  public String basePriceFormattedValue;
  public Double employeePrice;
  public String employeePriceFormattedValue;
  public String currencyIso;
  public String imgHiRes;
  public String imgLoRes;
  public String quantity;
  public String formattedValue;
  public ArrayList<Badge> badges;
  public boolean isRectangle;
  public String imageFormat;
  //used for order details
  public String productSize;
  public String productColor;
  //Use for communication page
  public String inStockNotificationId;
  public boolean displayConceptLogo;
  public boolean isheader = false;

  public Product() {
  }

  protected Product(Parcel in) {
    name = in.readString();
    code = in.readString();
    url = in.readString();
    variantType = in.readString();
    purchasable = in.readByte() != 0;
    isRectangle = in.readByte() != 0;
    concept = in.readString();
    priceFormattedValue = in.readString();
    basePriceFormattedValue = in.readString();
    employeePriceFormattedValue = in.readString();
    currencyIso = in.readString();
    imgHiRes = in.readString();
    imgLoRes = in.readString();
    quantity = in.readString();
    formattedValue = in.readString();
    productSize = in.readString();
    productColor = in.readString();
    inStockNotificationId = in.readString();
    displayConceptLogo = in.readByte() != 0;
    isheader = in.readByte() != 0;
  }

  @Override
  public String toString() {
    return "Product{" +
        "name='" + name + '\'' +
        ", code='" + code + '\'' +
        ", url='" + url + '\'' +
        ", variantType='" + variantType + '\'' +
        ", purchasable=" + purchasable +
        ", concept='" + concept + '\'' +
        ", price=" + price +
        ", priceFormattedValue='" + priceFormattedValue + '\'' +
        ", basePrice=" + basePrice +
        ", basePriceFormattedValue='" + basePriceFormattedValue + '\'' +
        ", employeePrice=" + employeePrice +
        ", employeePriceFormattedValue='" + employeePriceFormattedValue + '\'' +
        ", currencyIso='" + currencyIso + '\'' +
        ", imgHiRes='" + imgHiRes + '\'' +
        ", rectangleprodut='" + isRectangle + '\'' +
        ", imgLoRes='" + imgLoRes + '\'' +
        ", quantity='" + quantity + '\'' +
        ", formattedValue='" + formattedValue + '\'' +
        ", badges=" + badges +
        ", productSize='" + productSize + '\'' +
        ", productColor='" + productColor + '\'' +
        ", inStockNotificationId='" + inStockNotificationId + '\'' +
        ", displayConceptLogo=" + displayConceptLogo +
        ", isheader=" + isheader +
        '}';
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(name);
    parcel.writeString(code);
    parcel.writeString(url);
    parcel.writeString(variantType);
    parcel.writeByte((byte) (purchasable ? 1 : 0));
    parcel.writeByte((byte) (isRectangle ? 1 : 0));
    parcel.writeString(concept);
    parcel.writeString(priceFormattedValue);
    parcel.writeString(basePriceFormattedValue);
    parcel.writeString(employeePriceFormattedValue);
    parcel.writeString(currencyIso);
    parcel.writeString(imgHiRes);
    parcel.writeString(imgLoRes);
    parcel.writeString(quantity);
    parcel.writeString(formattedValue);
    parcel.writeString(productSize);
    parcel.writeString(productColor);
    parcel.writeString(inStockNotificationId);
    parcel.writeByte((byte) (displayConceptLogo ? 1 : 0));
    parcel.writeByte((byte) (isheader ? 1 : 0));
  }
}
