
package com.landmark.wearable.uat.parser.model.productdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fiveStartRatingCount",
    "fourStartRatingCount",
    "oneStartRatingCount",
    "threeStartRatingCount",
    "totalReview",
    "twoStartRatingCount"
})
public class ReviewSummary {

    @JsonProperty("fiveStartRatingCount")
    private Integer fiveStartRatingCount;
    @JsonProperty("fourStartRatingCount")
    private Integer fourStartRatingCount;
    @JsonProperty("oneStartRatingCount")
    private Integer oneStartRatingCount;
    @JsonProperty("threeStartRatingCount")
    private Integer threeStartRatingCount;
    @JsonProperty("totalReview")
    private Integer totalReview;
    @JsonProperty("twoStartRatingCount")
    private Integer twoStartRatingCount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("fiveStartRatingCount")
    public Integer getFiveStartRatingCount() {
        return fiveStartRatingCount;
    }

    @JsonProperty("fiveStartRatingCount")
    public void setFiveStartRatingCount(Integer fiveStartRatingCount) {
        this.fiveStartRatingCount = fiveStartRatingCount;
    }

    @JsonProperty("fourStartRatingCount")
    public Integer getFourStartRatingCount() {
        return fourStartRatingCount;
    }

    @JsonProperty("fourStartRatingCount")
    public void setFourStartRatingCount(Integer fourStartRatingCount) {
        this.fourStartRatingCount = fourStartRatingCount;
    }

    @JsonProperty("oneStartRatingCount")
    public Integer getOneStartRatingCount() {
        return oneStartRatingCount;
    }

    @JsonProperty("oneStartRatingCount")
    public void setOneStartRatingCount(Integer oneStartRatingCount) {
        this.oneStartRatingCount = oneStartRatingCount;
    }

    @JsonProperty("threeStartRatingCount")
    public Integer getThreeStartRatingCount() {
        return threeStartRatingCount;
    }

    @JsonProperty("threeStartRatingCount")
    public void setThreeStartRatingCount(Integer threeStartRatingCount) {
        this.threeStartRatingCount = threeStartRatingCount;
    }

    @JsonProperty("totalReview")
    public Integer getTotalReview() {
        return totalReview;
    }

    @JsonProperty("totalReview")
    public void setTotalReview(Integer totalReview) {
        this.totalReview = totalReview;
    }

    @JsonProperty("twoStartRatingCount")
    public Integer getTwoStartRatingCount() {
        return twoStartRatingCount;
    }

    @JsonProperty("twoStartRatingCount")
    public void setTwoStartRatingCount(Integer twoStartRatingCount) {
        this.twoStartRatingCount = twoStartRatingCount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
