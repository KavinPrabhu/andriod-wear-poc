package com.landmark.wearable.uat.network;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Arrays;
import java.util.Map;

/**
 * Created by tushard on 29/02/16.
 */
public class LmsReqResp {

    public String methodName;

    // Request Related
    public int methodType;     // Get/Post
    public String Url;
    public Map<String, String> reqHeaders;
    public Map<String, String> reqParams;
    public String reqBody;

    // Response Related
    public boolean connected;
    public String connectionStatus;
    public Integer respStatusCode;
    public Map<String, String> respHeaders;
    public byte[] respBody;
    public JsonNode respJSON;
    public String respBodyString;

    // Security related
    public String accessToken;
    public boolean accessTokenError;
    public String ErrorMessage = null;
    public String ErrorType = null;
    public String ErrorName;
    public long requestTime;


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("MethodName = ");
        builder.append(methodName);
        builder.append("\n");

        builder.append("methodType = ");
        builder.append(methodType);
        builder.append("\n");

        builder.append("Url = ");
        builder.append(Url);
        builder.append("\n");

        builder.append("reqBody = ");
        builder.append(reqBody);
        builder.append("\n");

        builder.append("reqHeaders = ");
        builder.append(Arrays.asList(reqHeaders));
        builder.append("\n");

        builder.append("reqParams = ");
        builder.append(Arrays.asList(reqParams));
        builder.append("\n");

        builder.append("connected = ");
        builder.append(connected);
        builder.append("\n");

        builder.append("connectionStatus = ");
        builder.append(connectionStatus);
        builder.append("\n");

        builder.append("respStatusCode = ");
        builder.append(respStatusCode);
        builder.append("\n");

        builder.append("respHeaders = ");
        builder.append(Arrays.asList(respHeaders));
        builder.append("\n");

        builder.append("respBodyString = ");
        builder.append(respBodyString);
        builder.append("\n");
        return builder.toString();
    }
}
