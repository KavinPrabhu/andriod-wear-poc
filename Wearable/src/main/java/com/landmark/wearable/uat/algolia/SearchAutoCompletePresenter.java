package com.landmark.wearable.uat.algolia;

import com.algolia.search.saas.AlgoliaException;
import com.algolia.search.saas.Client;
import com.algolia.search.saas.CompletionHandler;
import com.algolia.search.saas.Index;
import com.algolia.search.saas.Query;
import com.landmark.wearable.uat.Constants;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raghunathareddy on 1/11/17.
 * <p>
 * Presenter class which implements the business logic to be executed when an event
 * is triggered from SearchAutoComplete.
 */

public class SearchAutoCompletePresenter implements SearchAutoCompleteContract.EventHandler {

  // Algolia
  Client algClient;
  Index algIndex;
  private SearchAutoCompleteContract.SearchView mSearchView = null;
  private Categoryhelper objHelper;


  public SearchAutoCompletePresenter(SearchAutoCompleteContract.SearchView SearchView) {
    mSearchView = SearchView;
  }


  @Override
  public void onActivityResult(Categoryhelper mhelper) {

  }

  /*
   *  Function to get data from algolia based on user query
    */
  @Override
  public void onAlgoliaCall(Categoryhelper mHelper) {

    this.objHelper = mHelper;

    // --- Init algolia
    if (objHelper.tagFilter.length() == 0) {
      objHelper.tagFilter.put("lifestyle");
    }

    algClient = new Client(Constants.algoliaAppId, Constants.algoliaAPIKey);
    algIndex = algClient.initIndex(Constants.algoliaProductIndex);

    this.objHelper = mHelper;
    String[] facets = new String[1];
    facets[0] = "*";

    List<String> instocklist = new ArrayList<>();
    instocklist.add(Constants.algoliaInStock);
    if(!objHelper.disjunctiveFacets.contains("inStock")) {
      objHelper.disjunctiveFacets.add("inStock");
      objHelper.refinements.put("inStock", instocklist);
    }

    List<String> approvallist = new ArrayList<>();
    approvallist.add(Constants.algoliaApprovalStatus);
    if(!objHelper.disjunctiveFacets.contains("approvalStatus")) {
      objHelper.disjunctiveFacets.add("approvalStatus");
      objHelper.refinements.put("approvalStatus", approvallist);
    }

    if (objHelper.categoryCode != null && !objHelper.categoryCode.isEmpty()) {
      if (objHelper.disjunctiveFacets != null && objHelper.refinements != null) {
        List<String> categorylist;
        categorylist = objHelper.refinements.get("allCategories");
        if (categorylist == null) {
          categorylist = new ArrayList<>();
        }
        categorylist.add(objHelper.categoryCode);
        objHelper.refinements.put("allCategories", categorylist);

        if (!objHelper.disjunctiveFacets.contains("allCategories")) {
          objHelper.disjunctiveFacets.add("allCategories");
        }
      }
    }

    String[] attributesToRetrive = new String[1];
    attributesToRetrive[0] = "*";

    Query query;
    if (objHelper.searchQry != null && !objHelper.searchQry.isEmpty()) {
      query = new Query(objHelper.searchQry);
    } else {
      query = new Query();
    }
    query.setHitsPerPage(15);
    query.setPage(0);
    query.setFacets(facets);
    query.setMaxValuesPerFacet(100);
    query.setGetRankingInfo(true);
    JSONArray priceFilter = new JSONArray();
    priceFilter.put("price > 1");
    query.setNumericFilters(priceFilter);
//        query.setAttributesToRetrieve("*");
    query.setAttributesToRetrieve("name", "50WX50H", "concept", "manufacturerName", "price", "url",
        "facetName", "query");
    if (objHelper.tagFilter != null) {
      query.setTagFilters(objHelper.tagFilter);
    }

    // algIndex.searchASync(query, this);

    algIndex
        .searchDisjunctiveFacetingAsync(query, objHelper.disjunctiveFacets, objHelper.refinements,
            new CompletionHandler() {
              @Override
              public void requestCompleted(JSONObject results, AlgoliaException error) {
                if (error != null) {

                } else {
                  if (results == null) {
                    return;
                  }

                  // Set Data
                  objHelper.catFacetsArray = AlgoliaHelper
                      .getCategoryList(objHelper.categoryCode, results);
                  objHelper.productsMatchedArray = AlgoliaHelper
                      .parseProducts(results, false, "en");
                  //objHelper.productsMatchedArray = AlgoliaHelper.parseSearchProducts(results, false); used for landscape images
                  objHelper.totalProducts = AlgoliaHelper.getMatchedProducts(results);

                  mSearchView.algoliaCall(objHelper, objHelper.catFacetsArray);


                }
              }
            });

  }


}
