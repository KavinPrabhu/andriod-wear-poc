package com.landmark.wearable.uat.contract;

/**
 * Created by prasanth.p on 04/05/17.
 */

public interface ProductItemOnClickListener {

  void onItemSelected(String productId);

  void onCategoryItemSelected(String selection);
}
