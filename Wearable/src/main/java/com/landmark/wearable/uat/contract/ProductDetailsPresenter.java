package com.landmark.wearable.uat.contract;

import static com.landmark.wearable.uat.Util.convertToURLEncoded;

import android.util.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.landmark.wearable.uat.App;
import com.landmark.wearable.uat.network.HttpRequestHandler;
import com.landmark.wearable.uat.network.LmsReqResp;
import com.landmark.wearable.uat.network.NetworkRequestCallback;
import com.landmark.wearable.uat.parser.model.productdetails.ProductDetails;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by prasanth.p on 04/05/17.
 */

public class ProductDetailsPresenter implements ProductDetailsContract.EventHandler,
    NetworkRequestCallback {

  private String URL = App.BASE_URL + "v2/lifestyleae/en/products/";
  private static String FAVORITE_URL =
      App.BASE_URL + "v2/lifestyleae/en/customer/wishlist/current/";
  ProductDetailsContract.ProductDetailsView view;

  private final String MTD_GET_PROD_DETAILS = "getProductDetails";
  private final String ADD_FAVOURITE = "addFavouite";
  private final String REMOVE_FAVOURITE = "removeFavouite";
  private final String ADD_PRODUCT_TO_CART = "addProductToCart";

  public ProductDetailsPresenter(ProductDetailsContract.ProductDetailsView view) {
    this.view = view;
  }

  @Override
  public void getProductDetails(String productID) {
    HashMap<String, String> headers = new HashMap<>();
    URL = URL + productID + "?" + "access_token=" + App.accessToken
        + "&fields=FULL&userId=p3@gmail.com&appId=ANDROID&";
    final Call<ResponseBody> homePage = App.getApp().getService().getProductDetails(URL);
    final LmsReqResp lmsReqResp = new LmsReqResp();
    lmsReqResp.Url = URL;
    lmsReqResp.reqHeaders = headers;
    lmsReqResp.methodName = MTD_GET_PROD_DETAILS;
    makeRequest(this, lmsReqResp, homePage);
  }

  void makeRequest(NetworkRequestCallback callback, LmsReqResp reqResp, Call c) {
    HttpRequestHandler handler = new HttpRequestHandler(callback, reqResp, c);
    handler.sendRequest();
  }

  @Override
  public void NetworkResultAvailable(LmsReqResp lmsReqResp) {
    Log.i("ProductDetailsPresenter", "============= respJSON \n " + lmsReqResp.respJSON);
    if (view == null) {
      return;
    }
    switch (lmsReqResp.methodName) {
      case MTD_GET_PROD_DETAILS:
        if (lmsReqResp != null && lmsReqResp.respStatusCode == 200) {
          ObjectMapper objectMapper = new ObjectMapper();
          try {
            final ProductDetails productDetails = objectMapper
                .readValue(lmsReqResp.respJSON.toString(),
                    ProductDetails.class);
            view.onProductDetailsAvailable(productDetails);
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
        break;

      case ADD_FAVOURITE:
      case REMOVE_FAVOURITE:
        break;

      case ADD_PRODUCT_TO_CART:
        view.showProgressBar("");
        if (lmsReqResp != null && lmsReqResp.respStatusCode == 200) {
          view.showSuccessScreen();
        }
        break;
    }
    view.hideProgressBar();
  }

  public void addtoFavorite(String productcode) {
    Map<String, String> headers = new HashMap<String, String>();
    headers.put("Content-Type", "application/json");
    Map<String, String> params = new HashMap<String, String>();
    params.put("access_token", App.accessToken);
    params.put("appId", App.appId);
    params.put("productcode", productcode.replaceAll(" ", "%20"));

    LmsReqResp reqResp = new LmsReqResp();
    reqResp.reqParams = params;
    reqResp.Url = FAVORITE_URL + "addProduct";
    reqResp.methodName = ADD_FAVOURITE;

    final Call<ResponseBody> c = App.getApp().getService().addtoFavorite(reqResp.Url, params);
    makeRequest(this, reqResp, c);
  }

  public void removefromFavorite(String productcode) {
    Map<String, String> headers = new HashMap<String, String>();
    headers.put("Content-Type", "application/json");
    Map<String, String> params = new HashMap<String, String>();
    params.put("access_token", App.accessToken);
    params.put("appId", App.appId);
    params.put("productcode", productcode.replaceAll(" ", "%20"));

    LmsReqResp reqResp = new LmsReqResp();
    reqResp.reqParams = params;
    reqResp.Url = FAVORITE_URL + "removeProduct" + "?" + convertToURLEncoded(params);
    reqResp.methodName = REMOVE_FAVOURITE;
    final Call<ResponseBody> c = App.getApp().getService().removefromFavorite(reqResp.Url);
    makeRequest(this, reqResp, c);
  }

  public void addProducttoCart(String product, String quantity) {
    Map<String, String> params = new HashMap<String, String>();
    params.put("access_token", App.accessToken);
    params.put("appId", App.appId);
    params.put("code", product);
    params.put("qty", quantity);
    params.put("fields", "DEFAULT");

    LmsReqResp reqResp = new LmsReqResp();
    reqResp.methodName = "addToCart";
    reqResp.methodType = 1;
    reqResp.reqParams = params;
    reqResp.methodName = ADD_PRODUCT_TO_CART;
    reqResp.Url =
        App.BASE_URL + "v2/lifestyleae/en/users/" + App.email + "/carts/" + App.cartId
            + "/entries/";

    final Call<ResponseBody> c = App.getApp().getService()
        .addProducttoCart(reqResp.Url, reqResp.reqParams);
    makeRequest(this, reqResp, c);

  }
}
