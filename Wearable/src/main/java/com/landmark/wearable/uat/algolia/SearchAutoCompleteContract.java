package com.landmark.wearable.uat.algolia;

import com.landmark.wearable.uat.parser.model.algolia.Facet;

import java.util.ArrayList;

/**
 * Created by raghunathareddy on 1/11/17.
 * <p>
 * Contract class to register all the Interface Contracts of SearchAutoComplete.
 */

public interface SearchAutoCompleteContract {

  /**
   * The Presenter contract interface which defines all the events that will be
   * dispatched fom the UI and needs to be handled by the Presenter.
   */
  interface EventHandler {

    void onActivityResult(Categoryhelper mhelper);

    void onAlgoliaCall(Categoryhelper mhelper);

  }

  /**
   * View Interface which defines contract to update the UI.
   */
  interface SearchView {

    void algoliaCall(Categoryhelper mHelper, ArrayList<Facet> catFacetsArray);

  }

}
