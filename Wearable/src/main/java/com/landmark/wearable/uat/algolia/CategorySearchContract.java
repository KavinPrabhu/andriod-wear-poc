package com.landmark.wearable.uat.algolia;

import com.fasterxml.jackson.databind.JsonNode;
import com.landmark.wearable.uat.parser.model.algolia.Product;
import java.util.ArrayList;

/**
 * Created by pratikbehera on 1/5/17.
 * <p>
 * Contract class to register all the Interface Contracts of CategoryPg_SearchPg .
 */


public interface CategorySearchContract {

  /**
   * The Presenter contract interface which defines all the events that will be
   * dispatched fom the UI and needs to be handled by the Presenter.
   */
  interface EventHandler {

    void onActivityCreated(Categoryhelper mhelper);

    void onActivityResult(Categoryhelper mhelper);

    void onViewDestroyed();

    void onAlgoliaCall(Categoryhelper mhelper);

    void oncallFavourites();

    void onAddtoFavourite(String strCode);

    void onRemoveFavourite(String strCode);

  }

  /**
   * View Interface which defines contrach to update the UI.
   */
  interface CategoryPgView {

    void showApiError();

    void updateCategorydata(ArrayList<Product> mProducts, Categoryhelper mhelper);

    void CallNewCategoryFragment(Categoryhelper mhelper, String newSearchQuery);

    void allFavoriteCategory(JsonNode mNode);

    void addFavorite();

    void removeFavorite();

    void showSearchFailureError();
  }
}
