package com.landmark.wearable.uat;

import android.support.wearable.view.DelayedConfirmationView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by prasanth.p on 17/05/17.
 */

public class ProgressViewhandler implements DelayedConfirmationView.DelayedConfirmationListener {

  LinearLayout view;
  private DelayedConfirmationView mDelayedView;

  public ProgressViewhandler(LinearLayout view) {
    this.view = view;
    show();
  }


  @Override
  public void onTimerFinished(View view) {
    view.setVisibility(View.GONE);
  }

  @Override
  public void onTimerSelected(View view) {
    view.setVisibility(View.GONE);
  }

  public void hide() {
    view.setVisibility(View.GONE);
  }

  public void show() {
    mDelayedView = (DelayedConfirmationView) view.findViewById(R.id.delayed_confirm);
    mDelayedView.setVisibility(View.VISIBLE);
    mDelayedView.setListener(this);
    mDelayedView.reset();
    mDelayedView.start();
    mDelayedView.setTotalTimeMs(15000);
  }

  public void show(String message) {
    mDelayedView = (DelayedConfirmationView) view.findViewById(R.id.delayed_confirm);
    mDelayedView.setVisibility(View.VISIBLE);
    mDelayedView.setListener(this);
    mDelayedView.reset();
    mDelayedView.start();
    final TextView tv = (TextView) view.findViewById(R.id.textMessage);
    tv.setText(message);
    mDelayedView.setTotalTimeMs(15000);
  }
}
