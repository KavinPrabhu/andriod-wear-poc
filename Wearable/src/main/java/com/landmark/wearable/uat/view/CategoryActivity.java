package com.landmark.wearable.uat.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.wearable.view.WearableRecyclerView;
import android.util.Log;
import com.fasterxml.jackson.databind.JsonNode;
import com.landmark.wearable.uat.App;
import com.landmark.wearable.uat.Constants;
import com.landmark.wearable.uat.R;
import com.landmark.wearable.uat.adapter.CategoryAdapter;
import com.landmark.wearable.uat.algolia.CategoryAndSearchPresenter;
import com.landmark.wearable.uat.algolia.CategorySearchContract;
import com.landmark.wearable.uat.algolia.Categoryhelper;
import com.landmark.wearable.uat.contract.ProductItemOnClickListener;
import com.landmark.wearable.uat.parser.model.algolia.Product;
import com.landmark.wearable.uat.parser.model.algolia.RequiredFacet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by prasanth.p on 19/05/17.
 */

public class CategoryActivity extends Activity implements CategorySearchContract.CategoryPgView,
    ProductItemOnClickListener {

  WearableRecyclerView categoryList;
  CategoryAndSearchPresenter presenter;
  String CONCEPT = "lifestyle";
  Categoryhelper objhelper = new Categoryhelper();
  private int lastPageNo;
  private int clickPosition;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.category_page);
    categoryList = (WearableRecyclerView) findViewById(R.id.category_list);
    presenter = new CategoryAndSearchPresenter(this);
    onActivityCreated();
  }


  public void onActivityCreated() {
    Bundle bundle = getIntent().getExtras();

    if (bundle != null) {
      objhelper.fragment_url = bundle.getString("URL");
      objhelper.fragmentTag = bundle.getString("FragTag");

      // Omp - add concept
      if (!CONCEPT.isEmpty()) {
        String allconcepts = "";
        if (CONCEPT.equalsIgnoreCase("centrepoint")) {
          try {
            objhelper.tagFilter = new JSONArray(
                "[[\"splash\",\"babyshop\",\"lifestyle\",\"shoemart\"]]");
          } catch (JSONException e) {
            e.printStackTrace();
          }
        } else if (CONCEPT.equalsIgnoreCase("shoemart")) {
          objhelper.tagFilter.put("shoemart");
        } else if (CONCEPT.equalsIgnoreCase("homecentre")) {
          objhelper.tagFilter.put("homecentre");
        } else {
          objhelper.tagFilter.put(CONCEPT);
        }
      }

      objhelper.pageHits = 20;

      if (bundle.getSerializable("pageNo") != null) {
        lastPageNo = (Integer) bundle.getSerializable("pageNo");
        objhelper.pageHits = (lastPageNo + 1) * 20;
      }
      if (bundle.getSerializable("clickPosition") != null) {
        clickPosition = (Integer) bundle.getSerializable("clickPosition");
      }

      if (bundle.getBundle("extraBundle") != null) {
        objhelper.categoryCode = bundle.getBundle("extraBundle").getString("CategoryCode");
      } else {
        objhelper.categoryCode = bundle.getString("CategoryCode");
      }
      objhelper.searchQry = bundle.getString("SearchQuery");
      objhelper.algoliaSelectedIndex = bundle
          .getString("AlgoliaSelectedIndex", Constants.algoliaProductIndex);
      objhelper.minPrice = bundle.getInt("minPrice", 0);
      objhelper.maxPrice = bundle.getInt("maxPrice", 0);
      objhelper.nowMinPrice = bundle.getInt("nowMinPrice", 0);
      objhelper.nowMaxPrice = bundle.getInt("nowMaxPrice", 0);
      objhelper.requiredFacets = (ArrayList<RequiredFacet>) bundle.get("RequiredFacets");
      objhelper.disjunctiveFacets = bundle.getStringArrayList("DisjunctiveFacets");
      objhelper.refinements = (HashMap<String, List<String>>) bundle.get("Refinements");
      objhelper.filterApplied = bundle.getBoolean("FilterApplied", false);
      objhelper.colPerGrid = bundle.getInt("ColPerGrid", 2);


    } else {
      lastPageNo = -1;
      clickPosition = -1;
      objhelper.pageHits = 20;
    }

    objhelper.pageNo = 0;
    objhelper.pageDataEnd = false;
    objhelper.isPageLoading = false;
    // Show progress bar
    presenter.onActivityCreated(objhelper);
  }

  @Override
  public void showApiError() {

  }

  @Override
  public void updateCategorydata(ArrayList<Product> mProducts, Categoryhelper mhelper) {
    Log.i("TAG", "================== " + mProducts.size());
    CategoryAdapter adapter = new CategoryAdapter(mProducts, this);
    categoryList.setAdapter(adapter);
    categoryList.setHasFixedSize(true);
    categoryList.setLayoutManager(
        new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false));
  }

  @Override
  public void CallNewCategoryFragment(Categoryhelper mhelper, String newSearchQuery) {

  }

  @Override
  public void allFavoriteCategory(JsonNode mNode) {

  }

  @Override
  public void addFavorite() {

  }

  @Override
  public void removeFavorite() {

  }

  @Override
  public void showSearchFailureError() {

  }

  @Override
  public void onItemSelected(String productId) {
    Intent intent = new Intent(this, ProductDetailsActivity.class);
    intent.putExtra(App.EXTRA_PRODUCT_ID, productId);
    startActivity(intent);
  }

  @Override
  public void onCategoryItemSelected(String selection) {

  }
}
