package com.landmark.wearable.uat.contract;

import com.landmark.wearable.uat.parser.model.slots.Product;

import java.util.List;

/**
 * Created by prasanth.p on 27/04/17.
 */

public class ProductListContract {

  public interface ProductListView {

    void onProductListAvailable(List<Product> product);

    void hideProgressBar();
  }

  public interface EventHandler {

    void getProductList();

    void sendSignIn(String username, String password);
  }
}
