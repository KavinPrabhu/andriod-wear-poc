package com.landmark.wearable.uat.algolia;

import com.landmark.wearable.uat.parser.model.algolia.Facet;
import com.landmark.wearable.uat.parser.model.algolia.Product;
import com.landmark.wearable.uat.parser.model.algolia.RequiredFacet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;

/**
 * Created by pratikbehera on 1/5/17.
 */

public class Categoryhelper {

  public String fragment_url;
  public String fragmentTag;
  public String categoryCode;
  public String searchQry;
  public String newSearchQry;
  public String algoliaSelectedIndex;
  public Integer maxPrice;
  public int pageHits;
  public Integer nowMinPrice;
  public Integer nowMaxPrice;
  public Integer minPrice;
  public int productsMatched;
  public int pageNo;
  public ArrayList<RequiredFacet> requiredFacets;
  public ArrayList<Facet> facets;
  public List<String> disjunctiveFacets;
  public HashMap<String, List<String>> refinements;
  public boolean filterApplied;
  public boolean isavailable = false;
  public boolean pageDataEnd = false;
  public int colPerGrid;
  public JSONArray tagFilter = new JSONArray();
  public JSONArray priceFilter = new JSONArray();
  public boolean isPageLoading;
  public HashMap<String, List<String>> queryParams;
  public boolean useRectangularImage;
  public String categoryTitle;
  public String firstLevelCatCode;
  public String newCategoryCode;
  public String sortOption;
  public String lastUpdatedFacet;
  public String URL;
  public String TAG;
  public boolean izresetfilter;
  public String imageFormat;

  public ArrayList<Product> productsMatchedArray;
  public ArrayList<Facet> catFacetsArray;
  public Integer totalProducts;
  public ArrayList<String> recentSearchesArray;

  public enum DisplayPage {
    searchIntro,
    searchFound,
    searchNotFound
  }
}
