package com.landmark.wearable.uat.network;

import java.util.Map;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by prasanth.p on 26/04/17.
 */

public interface HttpService {

  @FormUrlEncoded
  @POST
  Call<ResponseBody> getHomePage(@Url String url,
      @FieldMap(encoded = true) Map<String, String> params);

  @GET
  Call<ResponseBody> getProductDetails(@Url String url);

  @POST
  @FormUrlEncoded
  Call<ResponseBody> addtoFavorite(@Url String url, @FieldMap Map<String, String> params);

  @DELETE
  @Headers("Content-Type: application/json")
  Call<ResponseBody> removefromFavorite(@Url String url);

  @POST
  @FormUrlEncoded
  Call<ResponseBody> addProducttoCart(@Url String url, @FieldMap Map<String, String> params);

  @POST
  Call<ResponseBody> doSignin(@Url String url, @Body RequestBody body);

  @GET
  Call<ResponseBody> getCartCount(@Url String url);

  @POST
  @FormUrlEncoded
  Call<ResponseBody> getCartId(@Url String url, @FieldMap Map<String, String> params);

}
