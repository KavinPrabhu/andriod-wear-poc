package com.landmark.wearable.uat.parser.model.algolia;

/**
 * Created by tushard on 04/05/16.
 */
public class Badge implements Comparable<Badge> {

  public String code;
  public String title;
  public String cssStyle;
  public Integer position;
  public boolean visibility;

  @Override
  public int compareTo(Badge another) {
    return this.position.compareTo(another.position);
  }
}
