package com.landmark.wearable.uat;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by prasanth.p on 18/05/17.
 */

public class Util {

  public static String convertToURLEncoded(Map<String, String> queryString) {
    StringBuilder sb = new StringBuilder();
    for (HashMap.Entry<String, String> e : queryString.entrySet()) {
      if (sb.length() > 0) {
        sb.append('&');
      }
      try {
        sb.append(URLEncoder.encode(e.getKey(), "UTF-8")).append('=')
            .append(URLEncoder.encode(e.getValue(), "UTF-8"));
      } catch (UnsupportedEncodingException e1) {
        e1.printStackTrace();
      }
    }
    return sb.toString();
  }

}
