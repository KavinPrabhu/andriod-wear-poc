package com.landmark.wearable.uat.parser.model.algolia;

import java.io.Serializable;

/**
 * Created by tushard on 27/03/16.
 */
public class RequiredFacet implements Comparable<RequiredFacet>, Serializable {

  public String className;
  public String facetTitle;
  public String facetName;
  public Integer priority;
  public Boolean collapsed;
  public Integer maxThreshold;

  @Override
  public int compareTo(RequiredFacet another) {
    return this.priority.compareTo(another.priority);
  }
}
