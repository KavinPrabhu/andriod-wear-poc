package com.landmark.wearable.uat.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.landmark.wearable.uat.App;
import com.landmark.wearable.uat.R;
import com.landmark.wearable.uat.adapter.BrandsAdapter;
import com.landmark.wearable.uat.contract.ProductItemOnClickListener;
import com.landmark.wearable.uat.contract.ProductListContract;
import com.landmark.wearable.uat.contract.ProductListPresenter;
import com.landmark.wearable.uat.parser.model.slots.Product;
import java.util.List;

/**
 * Created by prasanth.p on 16/04/17.
 */

public class ProductListActivity extends Activity implements ProductListContract.ProductListView,
    ProductItemOnClickListener {

  ProductListContract.EventHandler presenter;
  //ProgressDialog progressDialog;
//  ProgressViewhandler handler;
  RelativeLayout progressLyt;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    presenter = new ProductListPresenter(this);
    presenter.sendSignIn(App.email, App.password);
    setContentView(R.layout.top_brands);
        /*progressDialog = new ProgressDialog(this);
        progressDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
        progressDialog.setMessage("Loading product details");
        progressDialog.show();*/
    progressLyt = (RelativeLayout) findViewById(R.id.progresslayout);

    //handler = new ProgressViewhandler(progressLyt);
    TextView tv = (TextView) findViewById(R.id.text_item_search);
    tv.setClickable(true);
    tv.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(ProductListActivity.this, SearchActivity.class);
        startActivity(intent);
      }
    });

  }

  @Override
  public void onProductListAvailable(List<Product> product) {
    //progressDialog.dismiss();
    //handler.hide();
    progressLyt.setVisibility(View.GONE);

    Log.d("prasanth", "=============== size" + product.size());
    RecyclerView view = (RecyclerView) findViewById(R.id.brand_list);
    BrandsAdapter adapter = new BrandsAdapter(product, this);
    view.setAdapter(adapter);
    view.setHasFixedSize(true);
    view.setLayoutManager(
        new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false));
  }

  @Override
  public void hideProgressBar() {
    //progressDialog.dismiss();
    progressLyt.setVisibility(View.GONE);
    Toast.makeText(this, "An error occured while fetching details", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onItemSelected(String productId) {
    Intent intent = new Intent(this, ProductDetailsActivity.class);
    intent.putExtra(App.EXTRA_PRODUCT_ID, productId);
    startActivity(intent);
  }

  @Override
  public void onCategoryItemSelected(String selection) {

  }


}
