
package com.landmark.wearable.uat.parser.model.productdetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "approved",
    "averageRating",
    "baseProduct",
    "baseProductPK",
    "brand",
    "categories",
    "classifications",
    "code",
    "concept",
    "conceptDelivery",
    "description",
    "galleryImages",
    "images",
    "isClickCollectEligible",
    "keyBenefits",
    "loyaltyPoints",
    "name",
    "numberOfReviews",
    "potentialPromotions",
    "price",
    "purchasable",
    "reviewSummary",
    "stock",
    "summary",
    "threshold",
    "url",
    "variantType",
    "variants",
    "wishListProduct"
})
public class ProductDetails {

    @JsonProperty("approved")
    private Boolean approved;
    @JsonProperty("averageRating")
    private Integer averageRating;
    @JsonProperty("baseProduct")
    private String baseProduct;
    @JsonProperty("baseProductPK")
    private String baseProductPK;
    @JsonProperty("brand")
    private Brand brand;
    @JsonProperty("categories")
    private List<Category> categories = null;
    @JsonProperty("classifications")
    private List<Classification> classifications = null;
    @JsonProperty("code")
    private String code;
    @JsonProperty("concept")
    private Concept concept;
    @JsonProperty("conceptDelivery")
    private Boolean conceptDelivery;
    @JsonProperty("description")
    private String description;
    @JsonProperty("galleryImages")
    private List<GalleryImage> galleryImages = null;
    @JsonProperty("images")
    private List<Image_> images = null;
    @JsonProperty("isClickCollectEligible")
    private Boolean isClickCollectEligible;
    @JsonProperty("keyBenefits")
    private List<String> keyBenefits = null;
    @JsonProperty("loyaltyPoints")
    private Integer loyaltyPoints;
    @JsonProperty("name")
    private String name;
    @JsonProperty("numberOfReviews")
    private Integer numberOfReviews;
    @JsonProperty("potentialPromotions")
    private List<PotentialPromotion> potentialPromotions = null;
    @JsonProperty("price")
    private Price price;
    @JsonProperty("purchasable")
    private Boolean purchasable;
    @JsonProperty("reviewSummary")
    private ReviewSummary reviewSummary;
    @JsonProperty("stock")
    private Stock stock;
    @JsonProperty("summary")
    private String summary;
    @JsonProperty("threshold")
    private Integer threshold;
    @JsonProperty("url")
    private String url;
    @JsonProperty("variantType")
    private String variantType;
    @JsonProperty("variants")
    private List<Variant> variants = null;
    @JsonProperty("wishListProduct")
    private Boolean wishListProduct;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("approved")
    public Boolean getApproved() {
        return approved;
    }

    @JsonProperty("approved")
    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    @JsonProperty("averageRating")
    public Integer getAverageRating() {
        return averageRating;
    }

    @JsonProperty("averageRating")
    public void setAverageRating(Integer averageRating) {
        this.averageRating = averageRating;
    }

    @JsonProperty("baseProduct")
    public String getBaseProduct() {
        return baseProduct;
    }

    @JsonProperty("baseProduct")
    public void setBaseProduct(String baseProduct) {
        this.baseProduct = baseProduct;
    }

    @JsonProperty("baseProductPK")
    public String getBaseProductPK() {
        return baseProductPK;
    }

    @JsonProperty("baseProductPK")
    public void setBaseProductPK(String baseProductPK) {
        this.baseProductPK = baseProductPK;
    }

    @JsonProperty("brand")
    public Brand getBrand() {
        return brand;
    }

    @JsonProperty("brand")
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @JsonProperty("classifications")
    public List<Classification> getClassifications() {
        return classifications;
    }

    @JsonProperty("classifications")
    public void setClassifications(List<Classification> classifications) {
        this.classifications = classifications;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("concept")
    public Concept getConcept() {
        return concept;
    }

    @JsonProperty("concept")
    public void setConcept(Concept concept) {
        this.concept = concept;
    }

    @JsonProperty("conceptDelivery")
    public Boolean getConceptDelivery() {
        return conceptDelivery;
    }

    @JsonProperty("conceptDelivery")
    public void setConceptDelivery(Boolean conceptDelivery) {
        this.conceptDelivery = conceptDelivery;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("galleryImages")
    public List<GalleryImage> getGalleryImages() {
        return galleryImages;
    }

    @JsonProperty("galleryImages")
    public void setGalleryImages(List<GalleryImage> galleryImages) {
        this.galleryImages = galleryImages;
    }

    @JsonProperty("images")
    public List<Image_> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<Image_> images) {
        this.images = images;
    }

    @JsonProperty("isClickCollectEligible")
    public Boolean getIsClickCollectEligible() {
        return isClickCollectEligible;
    }

    @JsonProperty("isClickCollectEligible")
    public void setIsClickCollectEligible(Boolean isClickCollectEligible) {
        this.isClickCollectEligible = isClickCollectEligible;
    }

    @JsonProperty("keyBenefits")
    public List<String> getKeyBenefits() {
        return keyBenefits;
    }

    @JsonProperty("keyBenefits")
    public void setKeyBenefits(List<String> keyBenefits) {
        this.keyBenefits = keyBenefits;
    }

    @JsonProperty("loyaltyPoints")
    public Integer getLoyaltyPoints() {
        return loyaltyPoints;
    }

    @JsonProperty("loyaltyPoints")
    public void setLoyaltyPoints(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("numberOfReviews")
    public Integer getNumberOfReviews() {
        return numberOfReviews;
    }

    @JsonProperty("numberOfReviews")
    public void setNumberOfReviews(Integer numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }

    @JsonProperty("potentialPromotions")
    public List<PotentialPromotion> getPotentialPromotions() {
        return potentialPromotions;
    }

    @JsonProperty("potentialPromotions")
    public void setPotentialPromotions(List<PotentialPromotion> potentialPromotions) {
        this.potentialPromotions = potentialPromotions;
    }

    @JsonProperty("price")
    public Price getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Price price) {
        this.price = price;
    }

    @JsonProperty("purchasable")
    public Boolean getPurchasable() {
        return purchasable;
    }

    @JsonProperty("purchasable")
    public void setPurchasable(Boolean purchasable) {
        this.purchasable = purchasable;
    }

    @JsonProperty("reviewSummary")
    public ReviewSummary getReviewSummary() {
        return reviewSummary;
    }

    @JsonProperty("reviewSummary")
    public void setReviewSummary(ReviewSummary reviewSummary) {
        this.reviewSummary = reviewSummary;
    }

    @JsonProperty("stock")
    public Stock getStock() {
        return stock;
    }

    @JsonProperty("stock")
    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    @JsonProperty("threshold")
    public Integer getThreshold() {
        return threshold;
    }

    @JsonProperty("threshold")
    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("variantType")
    public String getVariantType() {
        return variantType;
    }

    @JsonProperty("variantType")
    public void setVariantType(String variantType) {
        this.variantType = variantType;
    }

    @JsonProperty("variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    @JsonProperty("wishListProduct")
    public Boolean getWishListProduct() {
        return wishListProduct;
    }

    @JsonProperty("wishListProduct")
    public void setWishListProduct(Boolean wishListProduct) {
        this.wishListProduct = wishListProduct;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
