package com.landmark.wearable.uat.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.view.WearableRecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.landmark.wearable.uat.App;
import com.landmark.wearable.uat.ProgressViewhandler;
import com.landmark.wearable.uat.R;
import com.landmark.wearable.uat.contract.ProductDetailsContract;
import com.landmark.wearable.uat.contract.ProductDetailsPresenter;
import com.landmark.wearable.uat.parser.model.productdetails.GalleryImage;
import com.landmark.wearable.uat.parser.model.productdetails.Image;
import com.landmark.wearable.uat.parser.model.productdetails.ProductDetails;
import com.landmark.wearable.uat.parser.model.productdetails.Variant;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by prasanth.p on 04/05/17.
 */

public class ProductDetailsActivity extends Activity implements
    ProductDetailsContract.ProductDetailsView {

  String productId;
  private ProductDetailsContract.EventHandler presenter;
  ProgressViewhandler handler;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    presenter = new ProductDetailsPresenter(this);
    setContentView(R.layout.product_details);
    productId = getIntent().getStringExtra(App.EXTRA_PRODUCT_ID);
    presenter.getProductDetails(productId);
    handler = new ProgressViewhandler((LinearLayout) findViewById(R.id.progresslayout));
    Button checkout = (Button) findViewById(R.id.checkout);
    showProgressBar("");
    checkout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showProgressBar("Adding product to cart...");
        presenter.addProducttoCart(productId, "1");
      }
    });
  }

  @Override
  public void showProgressBar(String message) {
    findViewById(R.id.progresslayout).setVisibility(View.VISIBLE);
    findViewById(R.id.contentLayout).setVisibility(View.GONE);
    if (message.contentEquals("")) {
      handler.show();
    } else {
      handler.show(message);
    }

  }

  @Override
  public void onProductDetailsAvailable(ProductDetails productDetails) {
    final List<GalleryImage> galleryImages = productDetails.getGalleryImages();
    ArrayList<String> productImages = new ArrayList<>();
    for (GalleryImage image : galleryImages) {
      for (Image img : image.getImages()) {
        if (img.getFormat().contentEquals("lmgMobileProduct") ||
            img.getFormat().contentEquals("lmgMobileProductRect")) {
          productImages.add(img.getUrl());
        }
      }
    }
    RecyclerView pager = (RecyclerView) findViewById(R.id.pager);
    pager.setAdapter(new CustomPagerAdapter(this, productImages));
    pager.setHasFixedSize(true);
    pager.setLayoutManager(new LinearLayoutManager(getBaseContext(),
        LinearLayoutManager.HORIZONTAL, false));
    TextView desc = (TextView) findViewById(R.id.description);
    TextView price = (TextView) findViewById(R.id.product_price);
    TextView productName = (TextView) findViewById(R.id.product_name);
    final String description = productDetails.getDescription();
    if (TextUtils.isEmpty(description)) {
      desc.setVisibility(View.GONE);
      findViewById(R.id.line_desc).setVisibility(View.GONE);
    } else {
      desc.setText(description);
    }
    price.setText(productDetails.getPrice().getFormattedValue());
    productName.setText(productDetails.getName());
    final List<Variant> variant = productDetails.getVariants();
    ArrayList variants = null;
    if (variant != null) {
      final Map<String, Object> additionalProperties = variant.get(0).getAdditionalProperties();
      variants = (ArrayList) additionalProperties.get("variants");
    }

    //Log.i("Prasanth", "==== variants size" + variants.size());
    final LinearLayout ll = (LinearLayout) findViewById(R.id.size_layout);
    if (variants != null) {
      for (Object data : variants) {
        LinkedHashMap d = (LinkedHashMap) data;
        final String size = (String) d.get("size");
        Button button = new Button(this);
        button.setText(size);
        button.setBackgroundColor(Color.GRAY);
        ll.addView(button);
        final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) button
            .getLayoutParams();
        layoutParams.weight = 1;
        layoutParams.height = 35;
        button.setPadding(0, 0, 0, 0);
        button.setTextSize(10);
      }
    } else {
      ll.setVisibility(View.GONE);
    }

    TextView stock = (TextView) findViewById(R.id.stockStatus);
    if (productDetails.getStock().getStockLevel() == 0) {
      stock.setVisibility(View.VISIBLE);
      stock.setText("Out of Stock ");

    }
  }

  @Override
  public void hideProgressBar() {
    handler.hide();
    findViewById(R.id.contentLayout).setVisibility(View.VISIBLE);
  }

  @Override
  public void showSuccessScreen() {
    Intent intent = new Intent(ProductDetailsActivity.this, ConfirmationActivity.class);
    intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE,
        ConfirmationActivity.SUCCESS_ANIMATION);
    intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE,
        getString(R.string.product_aded_to_cart));
    startActivity(intent);
    finish();
  }


  class CustomPagerAdapter extends WearableRecyclerView.Adapter<Vh> {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<String> urls;

    public CustomPagerAdapter(Context context, ArrayList<String> productImageUrls) {
      mContext = context;
      mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      urls = productImageUrls;
    }


    @Override
    public Vh onCreateViewHolder(ViewGroup parent, int viewType) {
      final LayoutInflater lf = LayoutInflater.from(getBaseContext());
      final View view = lf.inflate(R.layout.banner_image, parent, false);
      return new Vh(view);
    }


    @Override
    public void onBindViewHolder(Vh holder, int position) {
      Glide.with(mContext).load(urls.get(position))
          .thumbnail(0.5f)
          .crossFade()
          .into(holder.view);

      holder.favouriteView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          Integer res = (Integer) v.getTag();
          ImageView view = (ImageView) v;
          if (res == R.drawable.fav_notactive) {
            view.setImageResource(R.drawable.fav_active);
            v.setTag(R.drawable.fav_active);
            presenter.addtoFavorite(productId);
          } else {
            view.setImageResource(R.drawable.fav_notactive);
            v.setTag(R.drawable.fav_notactive);
            presenter.removefromFavorite(productId);
          }
        }
      });
    }


    @Override
    public int getItemCount() {
      Log.i("TAG", "getItemCount()" + urls.size());
      return urls.size();
    }
  }

  class Vh extends RecyclerView.ViewHolder {

    ImageView view;
    ImageView favouriteView;

    public Vh(View itemView) {
      super(itemView);
      view = (ImageView) itemView.findViewById(R.id.product_image);
      favouriteView = (ImageView) itemView.findViewById(R.id.favourite_img);
      favouriteView.setTag(R.drawable.fav_notactive);
    }
  }


}
