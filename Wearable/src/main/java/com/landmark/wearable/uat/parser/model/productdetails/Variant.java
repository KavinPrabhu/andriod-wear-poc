
package com.landmark.wearable.uat.parser.model.productdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "code",
    "stock",
    "color",
    "colorImageUrl",
    "hasSwatches",
    "isSelected"
})
public class Variant {

    @JsonProperty("code")
    private String code;
    @JsonProperty("stock")
    private Stock stock;
    @JsonProperty("color")
    private String color;
    @JsonProperty("colorImageUrl")
    private String colorImageUrl;
    @JsonProperty("hasSwatches")
    private Boolean hasSwatches;
    @JsonProperty("isSelected")
    private Boolean isSelected;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("stock")
    public Stock getStock() {
        return stock;
    }

    @JsonProperty("stock")
    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @JsonProperty("color")
    public String getColor() {
        return color;
    }

    @JsonProperty("color")
    public void setColor(String color) {
        this.color = color;
    }

    @JsonProperty("colorImageUrl")
    public String getColorImageUrl() {
        return colorImageUrl;
    }

    @JsonProperty("colorImageUrl")
    public void setColorImageUrl(String colorImageUrl) {
        this.colorImageUrl = colorImageUrl;
    }

    @JsonProperty("hasSwatches")
    public Boolean getHasSwatches() {
        return hasSwatches;
    }

    @JsonProperty("hasSwatches")
    public void setHasSwatches(Boolean hasSwatches) {
        this.hasSwatches = hasSwatches;
    }

    @JsonProperty("isSelected")
    public Boolean getIsSelected() {
        return isSelected;
    }

    @JsonProperty("isSelected")
    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
