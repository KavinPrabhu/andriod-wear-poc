package com.landmark.wearable.uat.network;

import android.util.Log;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prasanth.p on 16/12/16.
 */

public class HttpRequestHandler implements Callback<ResponseBody> {

    private static String TAG = HttpRequestHandler.class.getName();
    private LmsReqResp lmsReqResp;
    private NetworkRequestCallback callback;
    private Call<ResponseBody> futureCall;


    public HttpRequestHandler(NetworkRequestCallback callback, LmsReqResp lmsReqResp,
                              Call<ResponseBody> futureCall) {
        this.lmsReqResp = lmsReqResp;
        this.callback = callback;
        this.futureCall = futureCall;
    }

    /**
     * Invoked for a received HTTP response. <p> Note: An HTTP response may still indicate an
     * application-level failure such as a 404 or 500. Call {@link Response#isSuccessful()} to
     * determine if the response indicates success..
     */
    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        //final Pair<NetworkRequestCallback, LmsReqResp> pair = requestListener.remove(call);
        if (call.isCanceled()) {
            return;
        }
        String responseBodyString = "";
        lmsReqResp.respHeaders = null;
        try {
            if (response.isSuccessful()) {
                responseBodyString = response.body().string();
                lmsReqResp.respBody = responseBodyString.getBytes();
                Log.d(TAG, " onResponse - isSuccessful ");
                setResponseData(response.code(), responseBodyString);
                onSuccessResponse();
            } else {
                responseBodyString = response.errorBody().string();
                setResponseData(response.code(), responseBodyString);
                onResponseFailure();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        /*Log.i(TAG, " ######## onSuccessResponse response " + responseBodyString);
        Log.i(TAG, " ######## onResponse - response code " + response.code() +
                "   Method Name = " + lmsReqResp.methodName);*/
        sendResponse();

    }

    /**
     * Invoked when a network exception occurred talking to the server or when an unexpected
     * exception occurred creating the request or processing the response.
     */
    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.d(TAG, " onFailure ");
        t.printStackTrace();
        lmsReqResp.connectionStatus = "Error";
        lmsReqResp.respStatusCode = -1;
        sendResponse();
    }

    private void onSuccessResponse() {
        try {
            lmsReqResp.connected = true;
            lmsReqResp.connectionStatus = "Connected";
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.readTree(lmsReqResp.respBodyString);
            lmsReqResp.respJSON = rootNode;
            // Save access_token if we have one
            String accessToken = rootNode.path("access_token").asText();
            if (accessToken != null && !accessToken.isEmpty()) {
                lmsReqResp.accessToken = accessToken;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendResponse() {
        if (callback != null) {
            callback.NetworkResultAvailable(lmsReqResp);
            Log.d(TAG, "============= " + lmsReqResp.toString());
        }
    }

    public synchronized void sendRequest() {
        Log.i("sendRequest", " sendRequest complete URL " + lmsReqResp.Url);
        futureCall.enqueue(this);
    }

    private void onResponseFailure() {

        if (lmsReqResp.respStatusCode == 401) {
            //lmsReqResp.respStatusCode = -5;
            lmsReqResp.connectionStatus = "AuthFailure";
        } else {
            //lmsReqResp.respStatusCode = -4;
            lmsReqResp.connectionStatus = "ServerErr";
        }

        if (lmsReqResp.respBody != null && lmsReqResp.respBody.length > 0) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode rootNode = mapper.readTree(lmsReqResp.respBody);
                String tokenError = rootNode.path("errors").path(0).path("type").asText();
                if (tokenError != null && tokenError.equalsIgnoreCase("InvalidTokenError")) {
                    lmsReqResp.accessTokenError = true;
                }
                if (rootNode.path("errors").path(0).has("message")) {
                    String errorCode = rootNode.path("errors").path(0).path("message").asText();
                    //lmsReqResp.ErrorMessage = AppConfig.getErrormessages(errorCode);
                }
                if (rootNode.path("errors").path(0).has("type")) {
                    lmsReqResp.ErrorType = rootNode.path("errors").path(0).path("type").asText();
                    if (!lmsReqResp.ErrorType.equals("LMGVoucherRestrictionFailureError")) {
                        String newMsg = /*AppConfig.getErrormessages(lmsReqResp.ErrorType)*/ "";
                        if (newMsg != null && newMsg.equals(lmsReqResp.ErrorType) &&
                                lmsReqResp.ErrorMessage != null && !lmsReqResp.ErrorMessage.isEmpty()) {
                            // go with the message supplied from server
                        } else {
                            lmsReqResp.ErrorMessage = newMsg;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void setResponseData(int responseCode, String responseBodyString) {
        lmsReqResp.respStatusCode = responseCode;
        lmsReqResp.respBody = responseBodyString.getBytes();
        lmsReqResp.respBodyString = responseBodyString;
    }
}
