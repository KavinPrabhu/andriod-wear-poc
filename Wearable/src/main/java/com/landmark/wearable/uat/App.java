package com.landmark.wearable.uat;

import android.app.Application;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.landmark.wearable.uat.network.HttpService;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by prasanth.p on 26/04/17.
 */

public class App extends Application {

  public static final String EXTRA_PRODUCT_ID = "product_id";
  private static App application;
  public static String accessToken = "66b565f2-2489-4d29-b134-ca63efd04986";
  public static String appId = "ANDROID";
  public static String email = "dhoni7@gmail.com";
  public static String password = "123456";
  public static String cartId = "";
  public static String BASE_URL = "https://uat2.lifestyleshops.com/landmarkshopscommercews/";
  public static String client_id = "mobile_android";
  public static String client_secret = "F7LBBekbehGRQWpROIKJq";
  private ObjectMapper objectMapper;

  public ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  public HttpService getService() {
    return service;
  }

  private HttpService service;

  @Override
  public void onCreate() {
    super.onCreate();
    application = this;
    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    httpClient.connectTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS);
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("https://lms.com")
        .addConverterFactory(JacksonConverterFactory.create())
        .client(httpClient.build())
        .build();
    service = retrofit.create(HttpService.class);
    objectMapper = new ObjectMapper();
  }


  public static App getApp() {
    return application;
  }
}
