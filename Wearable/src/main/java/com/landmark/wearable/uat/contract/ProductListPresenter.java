package com.landmark.wearable.uat.contract;

import com.fasterxml.jackson.databind.JsonNode;
import com.landmark.wearable.uat.App;
import com.landmark.wearable.uat.Util;
import com.landmark.wearable.uat.network.HttpRequestHandler;
import com.landmark.wearable.uat.network.LmsReqResp;
import com.landmark.wearable.uat.network.NetworkRequestCallback;
import com.landmark.wearable.uat.parser.model.slots.Component;
import com.landmark.wearable.uat.parser.model.slots.Slot;
import com.landmark.wearable.uat.parser.model.startup.LoginResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by prasanth.p on 27/04/17.
 */

public class ProductListPresenter implements ProductListContract.EventHandler,
    NetworkRequestCallback {

  public static final String HOME_PAGE = "HomePage";
  public static final String SIGN_IN = "SignIn";
  public static final String GET_CART_ID = "GetCartId";
  private String URL = App.BASE_URL + "/v2/lifestyleae/en/cmsPage";
  ProductListContract.ProductListView view;

  public ProductListPresenter(ProductListContract.ProductListView view) {
    this.view = view;
  }


  @Override
  public void getProductList() {
    HashMap<String, String> headers = new HashMap<>();
    headers.put("access_token", App.accessToken);
    headers.put("appId", "ANDROID");
    headers.put("pageId", "lifestyleae-Homepage");
    final Call<ResponseBody> homePage = App.getApp().getService().getHomePage(URL, headers);
    final LmsReqResp lmsReqResp = new LmsReqResp();
    lmsReqResp.methodName = HOME_PAGE;
    lmsReqResp.reqHeaders = headers;
    makeRequest(this, lmsReqResp, homePage);
  }

  void makeRequest(NetworkRequestCallback callback, LmsReqResp reqResp, Call c) {
    HttpRequestHandler handler = new HttpRequestHandler(callback, reqResp, c);
    handler.sendRequest();
  }

  @Override
  public void NetworkResultAvailable(LmsReqResp lmsReqResp) {
    if (view == null) {
      return;
    }
    if (lmsReqResp == null) {
      return;
    }

    switch (lmsReqResp.methodName) {
      case HOME_PAGE:
        if (lmsReqResp.respStatusCode == 200) {
          final JsonNode slot1 = lmsReqResp.respJSON.get("slots").get(0);
          try {
            final Slot slot = App.getApp().getObjectMapper()
                .readValue(slot1.toString(), Slot.class);
            final List<Component> components = slot.getComponents();
            view.onProductListAvailable(components.get(0).getProducts());
          } catch (IOException e) {
            e.printStackTrace();
          }
        }else{
          view.hideProgressBar();
        }
        break;

      case SIGN_IN:
        if (lmsReqResp.respStatusCode == 200) {
          try {
            final LoginResponse loginResponse = App.getApp().getObjectMapper()
                .readValue(lmsReqResp.respBodyString, LoginResponse.class);
            App.accessToken = loginResponse.getAccessToken();
            getCartid(this);
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        break;

      case GET_CART_ID:
        App.cartId = "";
        if (lmsReqResp.respStatusCode  >= 200 && lmsReqResp.respStatusCode < 300) {
          App.cartId = lmsReqResp.respJSON.get("code").asText();
          getProductList();
        }
        break;
    }
  }

  public void sendSignIn(String username, String password) {
    Map<String, String> headers = new HashMap<String, String>();
    headers.put("Content-type", "application/x-www-form-urlencoded");
    Map<String, String> bodyParamsMap = new HashMap<>();
    bodyParamsMap.put("grant_type", "password");
    bodyParamsMap.put("client_id", App.client_id);
    bodyParamsMap.put("client_secret", App.client_secret);
    bodyParamsMap.put("username", username);
    bodyParamsMap.put("password", password);
    bodyParamsMap.put("appId", App.appId);

    LmsReqResp reqResp = new LmsReqResp();
    reqResp.methodName = SIGN_IN;
    reqResp.reqHeaders = headers;
    reqResp.methodType = 1;
    reqResp.reqParams = bodyParamsMap;
    reqResp.Url = App.BASE_URL+"oauth/token";
    reqResp.reqBody = Util.convertToURLEncoded(bodyParamsMap);

    final RequestBody requestBody = RequestBody
        .create(MediaType.parse("application/x-www-form-urlencoded"), reqResp.reqBody);
    final Call<ResponseBody> c = App.getApp().getService().doSignin(reqResp.Url, requestBody);
    makeRequest(this, reqResp, c);
  }


  public void getCartid(NetworkRequestCallback callback) {
    Map<String, String> params = new HashMap<String, String>();
    params.put("access_token", App.accessToken);
    params.put("fields", "DEFAULT");
    params.put("appId", App.appId);

    LmsReqResp reqResp = new LmsReqResp();
    reqResp.methodName = GET_CART_ID;
    reqResp.methodType = 1;
    reqResp.reqParams = params;
    reqResp.Url = "https://uat2.lifestyleshops.com/landmarkshopscommercews/v2/lifestyleae/en/users/"
        + App.email + "/carts";

    final Call<ResponseBody> c = App.getApp().getService().
        getCartId(reqResp.Url, params);
    makeRequest(callback, reqResp, c);
  }
}
